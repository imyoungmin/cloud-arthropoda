package require Tk

########################## Procedures #########################

#Function to evaluate and send a command.
proc submitCommand { } {
   set cmdStr [ .commandTextArea get 1.0 end ];		#Keep a copy of command text.
   set cmdStr [ string trim $cmdStr ];			#Remove trailing white spaces.
   .commandTextArea delete 1.0 end;			#Clear command text.
   
   if { [string length $cmdStr] > 0 } { 
   	outputLine "% $cmdStr" 1;			#Show command in console.
   	evaluate "$cmdStr";				#Evaluate command.
   }
}

#Function to print a line on the console.
proc outputLine { text { flag 0 } } {  
   .consoleTextArea configure -state normal;
   if { $flag == 0 } {
      .consoleTextArea insert end $text;
   } else {
      .consoleTextArea insert end $text "highlightline recent"; 
   }
   .consoleTextArea insert end "\n";
   .consoleTextArea see end;			#Make last character visible.
   .consoleTextArea configure -state disabled;
}

################################################################

#Set window location, size, and other properties.
wm geometry . 510x300+850+500;
wm title . "Animalia";
wm minsize . 510 300;
wm maxsize . 510 800;
. configure -background "#eaeaea"

#Creating label 1.
ttk::label .label1 -text "Input command:" -anchor w;
.label1 configure -background "#eaeaea";

#Creating the command input field.
ttk::scrollbar .scrollV1 -command ".commandTextArea yview" -orient vertical;
tk::text .commandTextArea -width 60 -height 2 -wrap char -yscrollcommand ".scrollV1 set";
.commandTextArea configure -highlightcolor "#abcdef";
.commandTextArea configure -highlightthickness 3;
bind .commandTextArea <Return> { submitCommand };
focus .commandTextArea;

#Creating label 2.
ttk::label .label2 -text "Output console:" -anchor w;
.label2 configure -background "#eaeaea"

#Creating the output console.
ttk::scrollbar .scrollV2 -command ".consoleTextArea yview" -orient vertical; 
ttk::scrollbar .scrollH2 -command ".consoleTextArea xview" -orient horizontal;
tk::text .consoleTextArea -width 60 -height 10 -wrap "char" -yscrollcommand ".scrollV2 set" -xscrollcommand ".scrollH2 set";
.consoleTextArea configure -highlightcolor "#abcdef";
.consoleTextArea configure -highlightthickness 3;
.consoleTextArea configure -state disabled;
.consoleTextArea tag configure highlightline -background "#F6F6C2" -spacing1 3 -spacing3 3;

#Setting up the layout.
grid .label1 		-column 0 -row 0 -sticky sw;
grid .scrollV1 		-column 1 -row 1 -sticky nsw;
grid .commandTextArea 	-column 0 -row 1 -sticky nwes
grid .label2 		-column 0 -row 2 -sticky sw;
grid .scrollV2 		-column 1 -row 3 -sticky nsw;
grid .scrollH2 		-column 0 -row 4 -sticky nwe;
grid .consoleTextArea 	-column 0 -row 3 -sticky nwes
grid columnconfigure . 0 -weight 1;
grid columnconfigure . 1 -weight 0;
grid rowconfigure . 0 -weight 0;
grid rowconfigure . 1 -weight 1;
grid rowconfigure . 2 -weight 0;
grid rowconfigure . 3 -weight 1;
grid rowconfigure . 4 -weight 0;
grid configure .label1 -padx 10 -pady 5;
grid configure .scrollV1 -padx "0 10" -pady "0 10";
grid configure .commandTextArea -padx "10 0" -pady "0 10";
grid configure .label2 -padx 10 -pady 5;
grid configure .scrollV2 -padx "0 10";
grid configure .scrollH2 -padx "10 0" -pady "0 10";
grid configure .consoleTextArea -padx "10 0";

#Attach quit procedure when user closes the Tk window.
bind . <Destroy> { quit }