//
//  ATcl.h
//  Arthropoda
//
//  Created by Luis Angel on 9/16/14.
//  Copyright (c) 2014 Luis Angel. All rights reserved.
//

#ifndef __Arthropoda__ATcl__
#define __Arthropoda__ATcl__

#include "Tcl.framework/Headers/tcl.h"
#include "tk.h"
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include "AManager.h"

#define STRLEN 100

typedef struct
{
	char name[ STRLEN ];
	int type;
	char *ptr;
} SETVAR;

class ATcl
{
private:
    static Tcl_Interp *aTclInterpreter;         //Global interpreter.
    static SETVAR setlist[];                    //User variables.
    
public:
    static int tclTkInit( Tcl_Interp *interp );
    static void tclTkEvents();
	static void linkUserVariables( Tcl_Interp *interp );
    static void outputMessage( const char *format, ... );
    static void executeCommand( const char *command );
    static int evaluateMethod( ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[] );
    static int quitMethod( ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[] );
    static int systemMethod( ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[] );
};

#endif /* defined(__Arthropoda__ATcl__) */
