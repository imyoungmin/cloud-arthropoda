//
//  AOde.cpp
//  Arthropoda
//
//  Created by Luis Angel on 9/18/14.
//  Copyright (c) 2014 Luis Angel. All rights reserved.
//

#include "AOde.h"

/***************************************************************************************************
 Constructor e initializer.
***************************************************************************************************/
AOde::AOde( double deltaT )
{
	///////////////// Initializing the ODE general features ////////////////////
    
	dInitODE();                                 // Initialize library.
	world = dWorldCreate();                     // Crate a new dynamics, empty world.
	space = dSimpleSpaceCreate( 0 );            // Create a new space for collision (independent).
	contactGroup = dJointGroupCreate( 0 );      // Create a joints container, without specifying size.
    
    // Simulation time step.
    timeStep = deltaT;
    dWorldSetGravity( world, 0.0, -9.81, 0 );	// Add gravity to this World.
    
	// Define error conrrection constants.
	dWorldSetERP( world, 0.2 );
	dWorldSetCFM( world, 1e-10 );
    
	// Set the velocity that interpenetrating objects will separate at.
	dWorldSetContactMaxCorrectingVel( world, 0.1 );
    
	// Set the safety area for contacts to be considered at rest.
	dWorldSetContactSurfaceLayer( world, 0.0001 );
    
	// Automatically disable objects that have come to a rest.
	//dWorldSetAutoDisableFlag( world, 1 );
    
	// Add damping to all bodies.
	dWorldSetDamping( world, 0.001, 0.001 );
    
	// Create a collision plane and add it to space. The next parameters are the
	// literal components of ax + by + cz = d.
	dCreatePlane( space, 0.0, 1.0, 0.0, 0.0 );
    
    /////////////////////////////// Create systems subject to ODE //////////////////////////////////
    
    spiderSystem = new ASpiderSystem( world, space, timeStep ); // Create spider.
    assert( AManager::addSystem( "spider", spiderSystem ) );    // And add its system to manager.

}

/***************************************************************************************************
 Destructor.
***************************************************************************************************/
AOde::~AOde()
{
	dJointGroupDestroy( contactGroup );		//Remove the contact joints.
    AManager::destroySystems();             //Remove joints from systems.
	dSpaceDestroy( space );					//Remove the space and all of its geoms.
	dWorldDestroy( world );					//Destroy all bodies and joints (not in a group).
    
    cout << "ODE environment has been destroyed" << endl;
}

/***************************************************************************************************
 Function to handle potential collisions between geometries.
***************************************************************************************************/
void AOde::nearCallBack( void *data, dGeomID o1, dGeomID o2 )
{
	int I;					//A temporary index for each contact.
	const int MAX_CONTACTS = 5;
    AOde* objReference = ( AOde* )data;
    
	// Get the dynamics body for each potentially colliding geometry.
	dBodyID b1 = dGeomGetBody( o1 );
	dBodyID b2 = dGeomGetBody( o2 );
    
	// Create an array of dContact objects to hold the contact joints.
	dContact contacts[ MAX_CONTACTS ];
    
	// Initialize contact structures.
	for( I = 0; I < MAX_CONTACTS; I++ )
	{
		contacts[I].surface.mode = dContactBounce | dContactSoftCFM;
		contacts[I].surface.mu = 0.1;		//0: frictionless, dInfinity: never slips.
		contacts[I].surface.mu2 = 0.1;			//Friction in direction 2 to mu.
		contacts[I].surface.bounce = 0.0001;		//0: not bouncy, 1: max. bouncyness.
		contacts[I].surface.bounce_vel = 5.0;	//Minimum incoming velocity for producting bouncyness.
		contacts[I].surface.soft_cfm = 0.00001;	//Softness for maintaining joint constraints.
	}
    
	// Now, do the actual collision test, passing as parameters the address of
	// a dContactGeom structure, and the offset to the next one in the list.
	if( int numc = dCollide( o1, o2, MAX_CONTACTS, &contacts[0].geom, sizeof(dContact) ) )
	{
        bool createContactJoints = true;        // Use this to determine if we need to create contact joints.

        // Store dContactGeom information in a vector.
        vector< dContactGeom > contactGeomInfo;
        for( I = 0; I < numc; I++ )
            contactGeomInfo.push_back( (contacts+I)->geom );
        
        // Proceed to create a ball-in-socket joint with spider claws in collision (if conditions are met).
        // Identifying which geom in the spider is in collision.
        unsigned char *spiderPtr = NULL;
        dGeomID envGeom = NULL;
        
        if( dGeomGetData( o1 ) != NULL && dGeomGetData( o2 ) == NULL )  // First geom belongs to spider, and second to env?
        {
            spiderPtr = ( unsigned char* )dGeomGetData( o1 );
            envGeom = o2;
        }
        else
        {
            if( dGeomGetData( o2 ) != NULL && dGeomGetData( o1 ) == NULL )  // First geom belongs to env, and second to spider?
            {
                spiderPtr = ( unsigned char* )dGeomGetData( o2 );
                envGeom = o1;
            }
        }
        
        if( spiderPtr != NULL )     // Skip self-collisions too.
        {
            int part, link;
            objReference->spiderSystem->decodeCollisionCode( *spiderPtr, &part, &link );
            createContactJoints = !objReference->spiderSystem->inCollision( part, link, envGeom, contactGeomInfo );
        }
        
		// Add contacts detected to the contact joint group as long as there is no joint attached already.
        if( createContactJoints )
        {
            for( I = 0; I < numc; I++ )
            {
                // Add contact joint by knowing its world, to a contact group. The last parameter
                // is the contact itself.
                dJointID c = dJointCreateContact( objReference->world, objReference->contactGroup, contacts + I );
                dJointAttach( c, b1, b2 );		//Attach two bodies to this contact joint.
            }
        }
	}
}

/***************************************************************************************************
 Function to perform the simulation loop for ODE.
***************************************************************************************************/
void AOde::simulationLoop()
{
	// Update velocities of end effectors in spider.
	spiderSystem->updateVelocities();
	
	// First, determine which geoms inside the space are potentially colliding.
	// The nearCallBack function will be responsible for analizing those potential collisions.
	// The second parameter indicates that we are sending a reference to current object.
	dSpaceCollide( space, (AOde *)this, &nearCallBack );
    
	// Next, advance the simulation, based on the step size given.
	dWorldStep( world, timeStep );
    
    // Perform additional simulation tasks on systems.
    spiderSystem->simulate();
    
	//Then, remove the temporary contact joints.
	dJointGroupEmpty( contactGroup );
    
}

/***************************************************************************************************
 Function to draw objects.
***************************************************************************************************/
void AOde::drawODEScene()
{
	///////////////////// Draw the collision plane ///////////////////
    ASpiderSystem* spiderPtr = ( ASpiderSystem* )AManager::getSystem( "spider" );   // A different way to access the spider system.
    AVector3D spiderPosition = spiderPtr->getPosition();
    
    double xMin = floor( spiderPosition[0] ) - 10.0;
    double xMax = floor( spiderPosition[0] ) + 10.0;
    double zMin = floor( spiderPosition[2] ) - 10.0;
    double zMax = floor( spiderPosition[2] ) + 10.0;
    double step = 0.1;
    
    glDisable( GL_LIGHTING );
    glColor3f( 0.5, 0.52, 0.55 );
    for( int I = 0; I < 200; I++ )
    {
        glBegin( GL_LINES );
        glVertex3d( xMin + I*step, 0.0, zMin );
        glVertex3d( xMin + I*step, 0.0, zMax );
        glEnd();
        
        glBegin( GL_LINES );
        glVertex3d( xMin, 0.0, zMin + I*step );
        glVertex3d( xMax, 0.0, zMin + I*step );
        glEnd();
    }
    glEnable( GL_LIGHTING );
    
    ///////////////// Draw ODE objects and creatures /////////////////
    AManager::drawSystems();
}







