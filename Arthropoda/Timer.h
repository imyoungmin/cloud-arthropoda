// Courtesy Alan Gasperini

#ifndef _TIMER_H_
#define _TIMER_H_

#include <sys/time.h>

class Timer
{
public:
	Timer();
    
	void Reset();
	float GetElapsedTime();
    
private:
	timeval cur_time;
    
};

inline void Timer::Reset()
{
	gettimeofday(&cur_time,0);
}

#endif  // _TIMER_H_
