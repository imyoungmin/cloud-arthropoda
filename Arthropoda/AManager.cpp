//
//  AManager.cpp
//  Arthropoda
//
//  Created by Luis Angel on 9/22/14.
//  Copyright (c) 2014 Luis Angel. All rights reserved.
//

#include "AManager.h"

/***************************************************************************************************
 Initializing static members.
***************************************************************************************************/
map< char*, ABaseSystem* > AManager::systemsDataBase = map< char*, ABaseSystem* >();

/***************************************************************************************************
 Destructor: free allocated memory for systems.
***************************************************************************************************/
void AManager::destroySystems()
{
    //Destroy joint groups.
    destroyJointGroupsInSystems();
    
    map< char*, ABaseSystem* >::iterator it;
    for( it = systemsDataBase.begin(); it != systemsDataBase.end(); it++ )
    {
        free( it->first );                  //Free name.
        free( it->second );                 //Free memory for System stored in database.
    }
    systemsDataBase.clear();                //Clear contents for systemsDataBase.
}

/***************************************************************************************************
 Function to add a new system into the database.
***************************************************************************************************/
bool AManager::addSystem( const char* name, ABaseSystem* systemPtr )
{
    if( systemPtr == NULL )                     //Check there is a system allocated.
        return false;
    
    if( name == NULL || strlen( name ) == 0 )   //Do not accept empty names.
        return false;
    
    if( checkDuplicateName( name ) )            //Does the name already exist?
        return false;
    
    char* sysName = new char[ strlen( name ) + 1 ]; //Allocate space for system name.
    assert( sysName );                          //Terminate if memory was not allocated.
    strcpy( sysName, name );
    
    systemsDataBase.insert( pair< char*, ABaseSystem* >( sysName, systemPtr ) );
    return true;
}

/***************************************************************************************************
 Function to get a system by its name
***************************************************************************************************/
ABaseSystem* AManager::getSystem( const char* name )
{
    if( name == NULL || strlen( name ) == 0 )
        return NULL;                            //Check name length.
    
    map< char*, ABaseSystem* >::iterator it;
    for( it = systemsDataBase.begin(); it != systemsDataBase.end(); it++ )
    {
        if( strcmp( name, it->first ) == 0 )    //Found it?
            return it->second;
    }
    return NULL;                                //Didn't find named system.
}

/***************************************************************************************************
 Function to draw systems in database.
***************************************************************************************************/
void AManager::drawSystems()
{
    map< char*, ABaseSystem* >::iterator it;
    for( it = systemsDataBase.begin(); it != systemsDataBase.end(); it++ )
        (it->second)->drawSystem();
}

/***************************************************************************************************
 Function to destroy the ODE joint group for each system.
***************************************************************************************************/
void AManager::destroyJointGroupsInSystems()
{
    map< char*, ABaseSystem* >::iterator it;
    for( it = systemsDataBase.begin(); it != systemsDataBase.end(); it++ )
        (it->second)->destroyJointGroup();
}

/***************************************************************************************************
 Function to check that if a given name exists in the systemsDataBase.
***************************************************************************************************/
bool AManager::checkDuplicateName( const char* name )
{
    map< char*, ABaseSystem* >::iterator it;
    for( it = systemsDataBase.begin(); it != systemsDataBase.end(); it++ )
    {
        if( strcmp( it->first, name ) == 0 )
            return true;            //The name already exists in database.
    }
    return false;                   //The name was not faound in database.
}