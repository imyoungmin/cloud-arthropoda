//
//  AHermiteSegment.h
//  Arthropoda
//
//  Created by Luis Angel on 10/12/14.
//  Copyright (c) 2014 Luis Angel. All rights reserved.
//

#ifndef __Arthropoda__AHermiteSegment__
#define __Arthropoda__AHermiteSegment__

#include "AMath.h"
#include <math.h>

/***************************************************************************************************
 Class that implements a Hermite curve segment between two control points.
***************************************************************************************************/

using namespace AMath;

class AHermiteSegment
{
private:
    AVector3D P0;               //Initial point.
    AVector3D P1;               //Ending point.
    AVector3D R0;               //Tangent at initial point.
    AVector3D R1;               //Tangent at ending point.
    
    AVector3D a;                //Vector of 'a' coefficients for the cubic: a_x, a_y, a_z.
    AVector3D b;                //Vector of 'b' coefficients.
    AVector3D c;                //Vector of 'c' coefficients.
    AVector3D d;                //Vector of 'd' coefficients.
    
public:
    AHermiteSegment( AVector3D p0 = zeroVector3D, AVector3D p1 = zeroVector3D, AVector3D r0 = zeroVector3D, AVector3D r1 = zeroVector3D );
    void setP0( AVector3D p0 );
    void setP1( AVector3D p1 );
    void setR0( AVector3D r0 );
    void setR1( AVector3D r1 );
    void initHermiteSegment();
    AVector3D P( double u );
};

#endif /* defined(__Arthropoda__AHermiteSegment__) */
