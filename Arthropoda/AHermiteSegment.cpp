//
//  AHermiteSegment.cpp
//  Arthropoda
//
//  Created by Luis Angel on 10/12/14.
//  Copyright (c) 2014 Luis Angel. All rights reserved.
//

#include "AHermiteSegment.h"

/***************************************************************************************************
 Constructor.
***************************************************************************************************/
AHermiteSegment::AHermiteSegment( AVector3D p0, AVector3D p1, AVector3D r0, AVector3D r1 )
{
    P0 = p0;        //Assign values to initial/ending points.
    P1 = p1;
    R0 = r0;        //Assign values to initial/ending tangents.
    R1 = r1;
    
    initHermiteSegment();
}

/***************************************************************************************************
 Function to modify initial point.
***************************************************************************************************/
void AHermiteSegment::setP0( AVector3D p0 )
{
    P0 = p0;
}

/***************************************************************************************************
 Function to modify ending point.
***************************************************************************************************/
void AHermiteSegment::setP1( AVector3D p1 )
{
    P1 = p1;
}

/***************************************************************************************************
 Function to modify initial tangent.
***************************************************************************************************/
void AHermiteSegment::setR0( AVector3D r0 )
{
    R0 = r0;
}

/***************************************************************************************************
 Function to modify ending tangent.
***************************************************************************************************/
void AHermiteSegment::setR1( AVector3D r1 )
{
    R1 = r1;
}

/***************************************************************************************************
 Function to compute the coefficients of the cubic curve, assuming that the parameter will go from
 0 to 1.
***************************************************************************************************/
void AHermiteSegment::initHermiteSegment()
{
    AVector3D diff = P1 - P0;
    a = R1 + R0 - 2.0 * diff;       //The coefficients.
    b = 3.0 * diff - 2.0 * R0 - R1;
    c = R0;
    d = P0;
}

/***************************************************************************************************
 Function to evaluate the Hermite curve segment at a paremeter u between 0 and 1. If u is not in the
 acceptable range, zero vector will be returned.
***************************************************************************************************/
AVector3D AHermiteSegment::P( double u )
{
    if( u < 0.0 || u > 1.0 )
        return zeroVector3D;
    
    AVector3D P = a*pow( u, 3 ) + b*pow( u, 2 ) + c*u + d;
    return P;
}





