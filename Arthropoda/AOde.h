//
//  AOde.h
//  Arthropoda
//
//  Created by Luis Angel on 9/18/14.
//  Copyright (c) 2014 Luis Angel. All rights reserved.
//

#ifndef __Arthropoda__AOde__
#define __Arthropoda__AOde__

#include "ADrawing.h"
#include "ode/ode.h"
#include "ATcl.h"
#include "AManager.h"
#include "ASpiderSystem.h"
#include <vector>
#include <exception>
#include <iostream>

using namespace std;

class ATcl;

class AOde
{
private:
    static void nearCallBack( void *data, dGeomID o1, dGeomID o2 );
    ASpiderSystem* spiderSystem;            // Actual spider system pointer.
    double timeStep;                        // Simulation timestep.
        
public:
    dWorldID world;                         // Dynamics world.
    dSpaceID space;                         // A space that defines collisions.
    dJointGroupID contactGroup;             // Group of contact joints for collision detection/handling.

    AOde( double deltaT );
    ~AOde();
    void simulationLoop();
    void drawODEScene();
};

#endif /* defined(__Arthropoda__AOde__) */
