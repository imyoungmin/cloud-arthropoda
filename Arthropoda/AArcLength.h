//
//  AArcLength.h
//  Arthropoda
//
//  Created by Luis Angel on 10/14/14.
//  Copyright (c) 2014 Luis Angel. All rights reserved.
//

#ifndef __Arthropoda__AArcLength__
#define __Arthropoda__AArcLength__

#include <vector>
#include <assert.h>
#include "AMath.h"

using namespace std;
using namespace AMath;

class AArcLengthTable
{
private:
    vector< pair< double, double >* >* table;   //A lookup table that stores pairs of parametric values u and arc length s.
    double totalLength;                         //Max distance recorded in the arc length table.
    
    void clean();
    
public:
    AArcLengthTable();
    ~AArcLengthTable();
    double fillOut( void* userData, int nSteps, int maxU, AVector3D(*functionPtr)( void*, double ) );
    double U( double s );
};

#endif /* defined(__Arthropoda__AArcLength__) */
