//
//  ABaseSystem.h
//  Arthropoda
//
//  Created by Luis Angel on 9/22/14.
//  Copyright (c) 2014 Luis Angel. All rights reserved.
//

#ifndef Arthropoda_ABaseSystem_h
#define Arthropoda_ABaseSystem_h

/***************************************************************************************************
 Abstract class to define System objects.
***************************************************************************************************/

#include "Tcl.framework/Headers/tcl.h"
#include "tk.h"
#include "ode/ode.h"
#include "AMath.h"
#include <vector>
#include <iostream>

using namespace std;
using namespace AMath;

class ABaseSystem
{
protected:
    
    //Struct to store ODE links.
    struct AObject
    {
        dBodyID body;						// The dynamics body.
        vector< dGeomID > geoms;			// Geometries representing this body.
    };
    
    //Struct to store ODE joints information.
    struct AJoint
    {
        dJointID joint;						// Dynamics joint.
        vector< double > desiredAngle;      // Where we want the joint to snap all the time.
        double radius;						// Joint radius (for drawing).
		AVector3D anchor;                   // Anchor position.
        dJointFeedback feedBack;			// Allocate space for joint feedback.
    };
    
    dJointGroupID jointGroup;				// Attach all joints to this group.
    
public:
    
    // Pure virtual function makes this class abstract: draw system.
    virtual void drawSystem() = 0;
    
    // Another pure virtual function: Tcl command to perform action on System.
    virtual int command( int objc, Tcl_Obj *CONST objv[] ) = 0;
    
    // Destroying the ODE joint group.
    void destroyJointGroup()
    {
        dJointGroupDestroy( jointGroup );
    }
    
};

#endif
