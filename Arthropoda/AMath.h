/***************************************************************************************************
My math library for vector operations.
***************************************************************************************************/
#pragma once

#include <cassert>
#include <sstream>
#include <cmath>
#include <vector>

using namespace std;

// Axes indices.
#define	X	0
#define Y	1
#define Z	2

// Mathematical constants.
#define PI				3.14159265358979323846
#define HALF_PI			PI/2.0
#define EPS				numeric_limits<double>::epsilon()
#define TWO_THIRDS		2.0/3.0
#define THREE_EIGHTS	3.0/8.0
#define ONE_SIXTH		1.0/6.0

namespace AMath
{
    /***********************************************************************************************
	 Function to generate a random number in a given range.
	 Inputs:
		from - lowest value.
		to - highest value.
	 Outputs:
		Random number in the range [from, to] inclusive.
	***********************************************************************************************/
	inline double random( double from, double to )
	{
		assert( to > from );	// Check from is always smaller than to.
		
		double base = static_cast<double>( rand() ) /RAND_MAX;
		double diff = to - from;
		
		return from + base*diff;
	}

    /***********************************************************************************************
     General matrix class. We use a template to define different types of matrices in compile time.
    ***********************************************************************************************/
    template< unsigned R, unsigned C >
    class AMatrix
    {
    protected:
        vector< double > v;              // Component values in the matrix.
        
    public:
        
        /*******************************************************************************************
         AMatrix class constructor.
        *******************************************************************************************/
        AMatrix( const double* data = NULL )
        {
            assert( R > 0 && C > 0 );                           //At least one element in the matrix.
            
            //Elements in the matrix must be given in row order.
			v = vector< double >( R*C, 0.0 );					// Fill R*C elements with zeros.
            
            //Copy elements into matrix.
            for( int i = 0; i < R; i++ )                        //First rows.
            {
                for( int j = 0; j < C; j++ )                    //Then columns.
                    v[ i*C + j ] = ( data != NULL )? data[ i*C + j ]: 0.0;
            }
        }
        
        /*******************************************************************************************
         Copy constructor.
        *******************************************************************************************/
        AMatrix( const AMatrix< R, C >& operand )
        {
			v = vector< double >( R*C, 0.0 );
			
            for( int I = 0; I < R; I++ )
            {
                for( int J = 0; J < C; J++ )
                    this->at( I, J ) = operand.at( I, J );
            }
        }
        
        /*******************************************************************************************
         Destructor.
        *******************************************************************************************/
        ~AMatrix()
        {
			v.clear();
        }
        
        /*******************************************************************************************
         Accessing operator.
        *******************************************************************************************/
        double at( unsigned I, unsigned J ) const
        {
            assert( I >= 0 && I < R && J >= 0 && J < C );     //Check indexes.
            return v[ I*C + J ];
        }
        
        /*******************************************************************************************
         Accessing with writing permission.
        *******************************************************************************************/
        double& at( unsigned I, unsigned J )
        {
            assert( I >= 0 && I < R && J >= 0 && J < C );     //Check indexes.
            return v[ I*C + J ];
        }
        
        /*******************************************************************************************
         Function to get the value of a component after accessing with the [].
        *******************************************************************************************/
        double operator []( unsigned I ) const
        {
            assert( I >= 0 && I < R*C );                    //Check range.
            return v[I];
        }
        
        /*******************************************************************************************
         Function to set the value of a component after accessing it with [].
        *******************************************************************************************/
        double& operator []( unsigned I )
        {
            assert( I >= 0 && I < R*C );                   //Check range.
            return v[I];
        }
        
        /*******************************************************************************************
         Overloading the assigment operator.
        *******************************************************************************************/
        const AMatrix< R, C > operator =( const AMatrix< R, C >& operand )
        {
            if( &operand != this )                    //Avoid self assignations.
            {
                for( int I = 0; I < R; I++ )          //Copy elements.
                {
                    for( int J = 0; J < C; J++ )
                        this->at( I, J ) = operand.at( I, J );
                }
            }
            
            return *this;
        }
        
        /*******************************************************************************************
         Overloading the addition of two matrices of the same size.
        *******************************************************************************************/
        AMatrix< R, C > operator +( const AMatrix< R, C >& operand ) const
        {
            AMatrix< R, C > sum;                    //Initialized by default to zeros.
            for( int I = 0; I < R; I++ )
            {
                for( int J = 0; J < C; J++ )
                    sum.at( I, J ) = this->at( I, J ) + operand.at( I, J );
            }
            
            return sum;
        }
        
        /*******************************************************************************************
         Overloading the subtraction of two matrices of the same size.
        *******************************************************************************************/
        AMatrix< R, C > operator -( const AMatrix< R, C >& operand ) const
        {
            AMatrix< R, C > negativeOperand = -AMatrix< R, C >( operand );   //Create a nonconstant version of operand.
            return *this + negativeOperand;
        }
        
        /*******************************************************************************************
         Overloading unary change of sign.
        *******************************************************************************************/
        AMatrix< R, C > operator -() const
        {
            return (*this) * (-1);
        }
        
        /*******************************************************************************************
         Overloading product by a scalar number.
        *******************************************************************************************/
        AMatrix< R, C > operator *( double scalar ) const
        {
            AMatrix< R, C > scaled;                   //Create matrix of same size as this one.
            
            for( int I = 0; I < R; I++ )
            {
                for( int J = 0; J < C; J++ )
                    scaled.at( I, J ) = this->at( I, J ) * scalar;    //Scale value.
            }
            return scaled;
        }
        
        /*******************************************************************************************
         Overloading product of two matrices with appropriate sizes.
        *******************************************************************************************/
        template< unsigned S, unsigned T >
        AMatrix< R, T > operator *( const AMatrix< S, T >& operand ) const
        {
            //Check size of operands.
            assert( C == S );          //Inner dimmensions must agree.
            
            //Create resulting product matrix.
            AMatrix< R, T > product;   //Intialized in zeros.
            
            for( int I = 0; I < R; I++ )
            {
                for( int J = 0; J < T; J++ )
                {
                    for( int K = 0; K < C; K++ )
                        product.at( I, J ) += this->at( I, K ) * operand.at( K, J );
                }
            }
            
            return product;
        }
        
        /*******************************************************************************************
         Overloading division by a scalar number.
        *******************************************************************************************/
        AMatrix< R, C > operator /( double scalar ) const
        {
            assert( scalar != 0.0 );                  //Avoid division by zero.
            
            AMatrix< R, C > divided;                  //Create matrix of same size as this one.
            
            for( int I = 0; I < R; I++ )
            {
                for( int J = 0; J < C; J++ )
                    divided.at( I, J ) = this->at( I, J )/scalar;
            }
            
            return divided;
        }
        
        /*******************************************************************************************
         Computing the transpose of this matrix.
        *******************************************************************************************/
        AMatrix< C, R > transpose() const
        {
            AMatrix< C, R > transpose;
            
            for( int I = 0; I < R; I++ )
            {
                for( int J = 0; J < C; J++ )
                    transpose.at( J, I ) = this->at( I, J );
            }
            
            return transpose;
        }
        
        /*******************************************************************************************
         Computing the determinant of this matrix.
         Use a recursive algorithm using cofactors.
        *******************************************************************************************/
        double determinant() const
        {
            assert( R == C );                   //Determinant is defined for square matrices only.
            
            if( R == 1 )                        //Trivial case.
                return this->at( 0, 0 );
            
            if( R == 2 )                        //Base case.
                return (this->at( 0, 0 ) * this->at( 1, 1 )) - (this->at( 0, 1 ) * this->at( 1, 0 ) );
            else
            {
                //Take the first row as pivot.
                double sign = 1;
                double determinant = 0;
                for( int c = 0; c < C; c++ )
                {
                    //Create a submatrix after removing row 0 and column c.
                    AMatrix< R-1, C-1 > subMatrix;
                    
                    for( int I = 1; I < R; I++ )            //Iterate over elements of this matrix to build the submatrix.
                    {
                        int cc = 0;
                        for( int J = 0; J < R; J++ )
                        {
                            if( J == c )                    //Skip pivot column.
                                continue;
                            
                            subMatrix.at( I-1, cc ) = this->at( I, J );
                            cc++;
                        }
                    }
                    
                    determinant += sign * this->at( 0, c ) * subMatrix.determinant();
                    sign *= -1;
                }
                
                return determinant;
            }
        }
        
        /*******************************************************************************************
         Function to compute the inverse of this matrix, as long as it is square and its determinant
         is non-zero.
        *******************************************************************************************/
        AMatrix< R, C > inverse() const
        {
            assert( R == C );                       //Check it is square.
            
            double det = this->determinant();       //Compute the determinant.
            
            assert( fabs( det ) > EPS );            //Verify the determinant is not close to zero.
            
            //Using the method of cofactors, compute the matrix of minors.
            double sign;
            AMatrix< R, C > Minors;
            for( int r = 0; r < R; r++ )            //Outer two loops are to decide which element we want to compute the determinant.
            {
                sign = ( r % 2 == 0 )? 1: -1;       //To create the checkerboard of signs.
                
                for( int c = 0; c < C; c++ )        //Then build a submatrix that excludes row r and column c.
                {
                    AMatrix< R-1, C-1 > M;
                    
                    int rr = 0;                     //rr defines row index for submatrix M.
                    for( int I = 0; I < R; I++ )
                    {
                        if( I == r )                //Skip this row?
                            continue;
                        
                        int cc = 0;                 //cc defines column index for submatrix M.
                        
                        for( int J = 0; J < C; J++ )
                        {
                            if( J == c )            //Skip this column?
                                continue;
                            
                            M.at( rr, cc ) = this->at( I, J );
                            cc++;
                        }
                        
                        rr++;
                    }
                    
                    //Compute the determinant of the submatrix and store it in the minors matrix.
                    Minors.at( r, c ) = sign * M.determinant();
                    sign *= -1;
                }
            }
            
            //The adjugate.
            AMatrix Adjugate( Minors.transpose() );
            
            //The inverse.
            return Adjugate / det;
        }
        
        /*******************************************************************************************
         Function to compute the Frobenius norm of this matrix.
        *******************************************************************************************/
        double norm() const
        {
            double sumOfSquares = 0.0;
            
            for( int I = 0; I < R; I++ )
            {
                for( int J = 0; J < C; J++ )
                    sumOfSquares += (this->at( I, J ))*(this->at( I, J ));
            }
            
            return sqrt( sumOfSquares );
        }
        
        /*******************************************************************************************
         Function to compute the dot product between two column vectors.
         Both this and the other operand must have the same number of rows.
        *******************************************************************************************/
        double dot( const AMatrix< R, C >& operand ) const
        {
            assert( C == 1 );               //Operation is defined only for column vectors.
            
            AMatrix< 1, 1 > product = (*this).transpose() * operand;
            return product[0];
        }
        
        /*******************************************************************************************
         Function to compute the cross product between two 3D column vectors.
        *******************************************************************************************/
        AMatrix< R, C > cross( const AMatrix< R, C >& operand ) const
        {
            assert( C == 1 && R == 3 );     //Check operation performs only on 3x1 matrices (vectors).
            
            double aArray[] = { 0, -v[2], v[1], v[2], 0, -v[0], -v[1], v[0], 0 };
            AMatrix< 3, 3 > A = AMatrix< 3, 3 >( aArray );
            return A * operand;
        }
        
        /*******************************************************************************************
         Functon to generate a transformed version of this vector.
         - Rot is a 4x4 matrix, laid down in row order.
         - Trans is a 4x1 matrix.
        *******************************************************************************************/
        AMatrix< R, C > transform( const double* Rot, const double* Trans ) const
        {
            AMatrix<3, 1> P;
            P[0] = Rot[0]*v[0] + Rot[1]*v[1] + Rot[2]*v[2] + Trans[0];
            P[1] = Rot[4]*v[0] + Rot[5]*v[1] + Rot[6]*v[2] + Trans[1];
            P[2] = Rot[8]*v[0] + Rot[9]*v[1] + Rot[10]*v[2] + Trans[2];
            
            return P;       //Function does not modify the original point.
        }
        
        /*******************************************************************************************
         Identify matrix.
        *******************************************************************************************/
        AMatrix< R, C > static identity()
        {
            assert( R > 0 && R == C );      // Validate dimensions (square).
            AMatrix< R, C > I;              // Since it is originally full of zeros.
            for( int i = 0; i < R; i++ )    // Put ones along the diagonal.
                I.at( i, i ) = 1.0;
            
            return I;
        }
        
        /*******************************************************************************************
         Function to convert the matrix into a string.
        *******************************************************************************************/
        std::string toString() const
        {
            std::string str = "";
            for( int I = 0; I < R; I++ )
            {
                str += "[\t";
                for( int J = 0; J < C; J++ )
                {
                    std::ostringstream s;
                    s << this->at( I, J );
                    str += s.str() + " \t";
                }
                
                str += "]\n";
            }
            return str;
        }
    };
    
    /***********************************************************************************************
     Auxiliary constructor to stop template recursion.
    ***********************************************************************************************/
    template<>
    class AMatrix< 0, 0 >
    {
    public:
        double at( unsigned, unsigned ) const;
        double& at( unsigned, unsigned );
        double determinant();
    };
    
    /***********************************************************************************************
     Function for scalar multiplication.
    ***********************************************************************************************/
    template< unsigned R, unsigned C >
    AMatrix< R, C > inline operator *( double scalar, const AMatrix< R, C >& matrix )
    {
        return ( matrix * scalar );
    }
	
	/***********************************************************************************************
	 Function to generate 3D rotation matrices.
	***********************************************************************************************/
	AMatrix< 3, 3 > inline get3DRotation( double theta, unsigned int axis )
	{
		double c = cos( theta );		// Store cosine and sine to avoid redundancy.
		double s = sin( theta );
		
		switch( axis )
		{
			case 0:						// Rotation around x-axis.
			{
				double rx[] = {  1,  0,  0,
								 0,  c, -s,
								 0,  s,  c  };
				return AMatrix< 3, 3 >( rx );
			}
			case 1:						// Rotation around y-axis.
			{
				
				double ry[] = {  c,  0,  s,
								 0,  1,  0,
								-s,  0,  c  };
				return AMatrix< 3, 3 >( ry );
			}
			case 2:						// Rotation around z-axis.
			{
				double rz[] = {	 c, -s,  0,
								 s,  c,  0,
								 0,  0,  1  };
				return AMatrix< 3, 3 >( rz );
			}
			default:
				assert( false );		// Error: only axes 0 through 2 (i.e. x through z).
		}
		
	}
    
    /***********************************************************************************************
     Wrapper for 3x1 column vectors.
    ***********************************************************************************************/
    typedef AMatrix< 3, 1 > AVector3D;
    
    const AVector3D zeroVector3D;        //The 3D zero vector.
	
	/***********************************************************************************************
	 Quaternions
	***********************************************************************************************/
	class AQuaternion
	{
	private:
		double s;						// Scalar value.
		AVector3D v;					// Vector value.
		
	public:
		/**
		 * @brief Constructor with 2 parameters.
		 * @param sp scalar parameter.
		 * @param vp vector parameter.
		 */
		AQuaternion( double sp = 0, const AVector3D& vp = zeroVector3D )
		{
			s = sp;
			v = vp;
		}
		
		
		/**
		 * @brief Copy constructor.
		 * @param q constant quaternion.
		 */
		AQuaternion( const AQuaternion& q )
		{
			s = q.S();
			v = q.V();
		}
		
		
		/**
		 * @brief Access the scalar element of the quaternion.
		 * @return constant scalar component of the quaterion.
		 */
		double S() const
		{
			return s;
		}
		
		
		/**
		 * @brief Access the scalar element of the quaternion.
		 * @return modifiable reference to scalar component of the quaternion.
		 */
		double& S()
		{
			return s;
		}
		
		
		/**
		 * @brief Access the vector element of the quaternion.
		 * @return constant vector component of the quaternion.
		 */
		AVector3D V() const
		{
			return v;
		}
		
		/**
		 * @brief Access the vector element of the quaternion.
		 * @return modifiable vector component of the quaternion.
		 */
		AVector3D& V()
		{
			return v;
		}
		
		
		/**
		 * @brief Quaternion assignment.
		 * @param q the quaternion to make this quaternion equal to.
		 * @return a reference to this quaternion for chain assignments.
		 */
		const AQuaternion operator=( const AQuaternion& q )
		{
			if( &q != this )		// Prevent self-assignments.
			{
				s = q.S();
				v = q.V();
			}
			
			return *this;
		}
		
		
		/**
		 * @brief Quaternion addition.
		 * @param q the other quaternion summand.
		 * @return result of addition.
		 */
		const AQuaternion operator+( const AQuaternion& q ) const
		{
			AQuaternion sum;
			sum.S() = s + q.S();
			sum.V() = v + q.V();
			
			return sum;
		}
		
		
		/**
		 * @brief Quaternion subtraction.
		 * @param q the other operand quaternion.
		 * @return result from subtraction.
		 */
		const AQuaternion operator-( const AQuaternion& q )
		{
			AQuaternion dif;
			dif.S() = s - q.S();
			dif.V() = v - q.V();
			
			return dif;
		}
		
		
		/**
		 * @brief Quaternion scalar product.
		 * @param t scalar factor.
		 * @return scaled quaternion.
		 */
		const AQuaternion operator*( double t )
		{
			AQuaternion sproduct( s*t, v*t );
			return sproduct;
		}
		
		
		/**
		 * @brief Quaternion scalar quotient.
		 * @param t scalar divisor.
		 * @return scaled quaternion.
		 */
		const AQuaternion operator/( double t )
		{
			assert( t != 0 );		// Avoid division by zero.
			
			AQuaternion quotient;
			quotient.S() = s/t;
			quotient.V() = v/t;
			
			return quotient;
		}
		
		
		/**
		 * @brief Quaternion product.
		 * @param q the other quaternion factor.
		 * @return quaternion product.
		 */
		const AQuaternion operator*( const AQuaternion& q ) const
		{
			AQuaternion product;
			product.S() = s*q.S() - v.dot( q.V() ) ;
			product.V() = s*q.V() + q.S()*v + v.cross( q.V() );
			
			return product;
		}
		
		
		/**
		 * @brief Computes the norm of this quaternion.
		 * @return this quaternion Euclidean norm.
		 */
		double norm()
		{
			double sum = s*s + v.dot( v );
			return sqrt( sum );
		}
		
		
		/**
		 * @brief Computes the inverse quaternion.
		 * @return this quaternion inverse.
		 */
		const AQuaternion inverse()
		{
			double normSquare = s*s + v.dot( v );
			assert( normSquare > EPS );			// Check length is larger than zero.
			
			AQuaternion inv( s, -v );
			return inv/normSquare;
		}
		
		
		/**
		 * @brief Rotates a 3D vector.
		 * @remarks This quaternion should be a valid rotation quaternion (i.e. by calling 
		 * AQuaternion::Rotation) in order for the rotation to work.
		 * @param vector AVector3D vector.
		 * @return rotated vector.
		 */
		const AVector3D rotateVector( const AVector3D& vector )
		{
			AQuaternion t( 0, vector );
			AQuaternion r = this->operator*( t * this->inverse() );
			return r.V();
		}
		
		
		/**
		 * @brief Constructs a rotation quaternion from an angle and axis.
		 * @remarks The rotation axis need not be unit. It will be normalized within this function.
		 * @param angle angle in radians.
		 * @param axis AVector3D unit axis.
		 */
		static const AQuaternion Rotation( double angle, const AVector3D& axis )
		{
			double axisLength = axis.norm();
			assert( axisLength > EPS );			// Zero axes are not accepted.
			
			AVector3D unitAxis = axis / axisLength;
			
			AQuaternion q( cos(angle/2), sin(angle/2) * unitAxis );
			return q;
		}
		
	};
	
	
	/**
	 * @brief Scalar multiplication of a quaternion with the scalar being the first factor.
	 * @remarks Compare with the same overloaded * but within the AQuaternion class.
	 * @param t scalar factor.
	 * @param q quaternion factor.
	 * @return resulting scalar product.
	 */
	inline const AQuaternion operator*( double t, const AQuaternion& q )
	{
		return q * t;
	}
	
	
	/**
	 * @brief Get a rotation matrix from a quaternion.
	 * @remarks The quaternion must be unit-length (e.g. like one obtained by AQuaternion::Rotation).
	 * @param q unit quaternion.
	 * @return a 3x3 AMatrix.
	 */
	inline const AMatrix< 3, 3 > getMatrixFromQuaternion( const AQuaternion& q )
	{
		double s = q.S();			// Get variables for quick use.
		double x = q.V()[X];
		double y = q.V()[Y];
		double z = q.V()[Z];
		
		double m[] = { 1-2*y*y-2*z*z,   2*x*y-2*s*z,   2*x*z+2*s*y,
						 2*x*y+2*s*z, 1-2*x*x-2*z*z,   2*y*z-2*s*x,
						 2*x*y-2*s*y,   2*y*z+2*s*x, 1-2*x*x-2*y*y  };
		
		return AMatrix< 3, 3 >( m );
	}
	
	
	/**
	 * @brief Get a quaternion from a rotation matrix.
	 * @param M a 3x3 AMatrix.
	 * @return a unit quaternion.
	 */
	inline const AQuaternion getQuaternionFromMatrix( const AMatrix< 3, 3 > M )
	{
		double s = sqrt( M.at(0, 0) + M.at(1, 1) + M.at(2, 2) + 1 )/2.0;
		double v[3];
		v[X] = sqrt( M.at(0, 0) + 1 - 2*s*s )/2.0;
		v[Y] = sqrt( M.at(1, 1) + 1 - 2*s*s )/2.0;
		v[Z] = sqrt( M.at(2, 2) + 1 - 2*s*s )/2.0;
		
		return AQuaternion( s, AVector3D( v ) );
	}
	
}

















