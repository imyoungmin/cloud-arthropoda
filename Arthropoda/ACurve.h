//
//  ACurve.h
//  Arthropoda
//
//  Created by Luis Angel on 10/14/14.
//  Copyright (c) 2014 Luis Angel. All rights reserved.
//

#ifndef __Arthropoda__ACurve__
#define __Arthropoda__ACurve__

#include "AHermiteSegment.h"
#include "AMath.h"
#include "AArcLength.h"
#include "GL/freeglut.h"
#include "GL/glut.h"
#include <vector>
#include <iostream>

/***************************************************************************************************
 Class to implement a generalized curve composed of one or more Hermite segments.
***************************************************************************************************/

using namespace std;
using namespace AMath;

namespace ACurveNS
{
    
    enum CurveTypes                                         // Types of curves.
    {
        CURVE_NONE,                                         // No curve assigned.
        CURVE_ARC,                                          // Arc.
        CURVE_ELLIPSE,                                      // Ellipse.
    };
    
    struct CurveData                                        // Store last curve data.
    {
        CurveTypes curveType;                               // Type of last set-up curve.
        AVector3D p0;                                       // Initial point.
        AVector3D r0;                                       // Initial tangent (not applicable to ellipse).
        AVector3D r1;                                       // Ending tangent (not applicable to ellipse).
        AVector3D direction;								// Curve direction.
		AVector3D normal;									// An ellipse normal.
        double v0;                                          // Constant velocity.
        double easeIn;                                      // Easing in percentage.
        double easeOut;                                     // Easing out percentage.
    };

    class ACurve
    {
    protected:
        
        vector< AHermiteSegment > hermiteSegments;          // Individual Hermite segments.
        int numberOfSegments;                               // Number of Hermite segments.
        double ta, tb;                                      // Time marks for stop accelerating/start decelerating.
        double totalTime;                                   // Total time to traverse the whole curve.
        double da, db;                                      // Distance portions for easing in/out.
        double totalDistance;                               // Curve's total arc length.
        double constSpeed;                                  // Base constant speed.
        AArcLengthTable arcLength;                          // Arc length table.
        
        double initCurve( double v0, double a, double b );
        
    public:
        
        ACurve();
        void resetAndDisable();
        AVector3D P( double u );
        static AVector3D Pstatic( void* userData, double u );
        double U( double s );
        double ease( double t );
        void drawCurve( const double* R, const double* T );
        
        //Specific curve types.
        double setEllipse( AVector3D p0, AVector3D dir, double h, AVector3D n, double v0, double easeInD, double easeOutD );
        double setArc( AVector3D p0, AVector3D dir, AVector3D r0, AVector3D r1, double v0, double easingInD, double easingOutD );
        CurveData getLastCurveData();
        
    private:
        
        CurveData curveData;                                // Data for last set-up curve.
    };
}

#endif /* defined(__Arthropoda__ACurve__) */
