//
//  AManager.h
//  Arthropoda
//
//  Created by Luis Angel on 9/22/14.
//  Copyright (c) 2014 Luis Angel. All rights reserved.
//

#ifndef __Arthropoda__AManager__
#define __Arthropoda__AManager__

/***************************************************************************************************
 Class that controls Systems registered in the simulation.
***************************************************************************************************/

#include "ABaseSystem.h"
#include <map>
#include <assert.h>

using namespace std;

class AManager
{
private:
    static map< char*, ABaseSystem* > systemsDataBase;
    static bool checkDuplicateName( const char* name );
    
public:
    static void destroySystems();
    static bool addSystem( const char* name, ABaseSystem* systemPtr );
    static ABaseSystem* getSystem( const char* name );
    static void drawSystems();
    static void destroyJointGroupsInSystems();
};

#endif /* defined(__Arthropoda__AManager__) */
