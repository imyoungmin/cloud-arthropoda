//
//  AArcLength.cpp
//  Arthropoda
//
//  Created by Luis Angel on 10/14/14.
//  Copyright (c) 2014 Luis Angel. All rights reserved.
//

#include "AArcLength.h"

/***************************************************************************************************
 Constructor.
***************************************************************************************************/
AArcLengthTable::AArcLengthTable()
{
    totalLength = 0.0;
    table = NULL;
}

/***************************************************************************************************
 Destructor.
***************************************************************************************************/
AArcLengthTable::~AArcLengthTable()
{
    clean();
}

/***************************************************************************************************
 Function to clear the contents of the arc length table.
***************************************************************************************************/
void AArcLengthTable::clean()
{
    if( table != NULL )
    {
        for( int I = 0; I < table->size(); I++ )    //Traverse each table row and erase its pair.
            delete table->at( I );
        delete table;                               //Finally, delete table's memory.
    }
    
    table = NULL;
}

/***************************************************************************************************
 Function to fill out the arc length table:
 - userData is a pointer to a private method to evaluate a curve.
 - nSteps is the number of evaluations of the curve from u = 0 to u = 1 (i.e. per segment).
 - maxU is the maximum value for u parameter (at least 1).
 - functionPtr is a pointer to a function to evaluate the curve at every u value.
***************************************************************************************************/
double AArcLengthTable::fillOut( void* userData, int nSteps, int maxU, AVector3D(*functionPtr)( void*, double ) )
{
    assert( maxU >= 1 );
    assert( nSteps >= 2 );      //At least 2 evaluations per segment.
    
    clean();                    //Clean table.
    table = new vector< pair< double, double>* >;       //Create a new table.
    
    AVector3D P0, P1, D;        //Points and vector resulting from evaluating the curve.
    totalLength = 0.0;
    double stepSize = 1.0 / ( nSteps - 1.0 );
    double u = 0.0;
    
    //First evaluation.
    P0 = (*functionPtr)( userData, u );
    
    for( int segment = 0; segment < maxU; segment++ )
    {
        P1 = (*functionPtr)( userData, segment );       //Obtain initial point (and makes sure it is interpolated).
        D = P1 - P0;
        totalLength += D.norm();
        
        table->push_back( new pair< double, double>( segment, totalLength ) );
        
        P0 = P1;
        u = segment + stepSize;                         //Fix any numerical drift by making sure to interpolate control points.
        
        for( int I = 1; I < nSteps - 1; I++ )   //Once it reaches the maximum value for u, stop evaluating.
        {
            P1 = (*functionPtr)( userData, u );
            D = P1 - P0;    //Difference vector.
            totalLength += D.norm();                        //Accumulate distance.
            
            table->push_back( new pair<double, double>( u, totalLength ) );
            
            P0 = P1;
            u += stepSize;
        }
    }
    
    //Finally, add last step because u may be >= maxU.
    P1 = (*functionPtr)( userData, maxU );
    D = P1 - P0;
    totalLength += D.norm();
    table->push_back( new pair< double, double >( maxU, totalLength ) );
    
    return totalLength;
}

/***************************************************************************************************
 Function to approximate the value for parameter u given arc length s by using binary search.
***************************************************************************************************/
double AArcLengthTable::U( double s )
{
    assert( table );                        //Check that table has been filled out.
    assert( s >= 0.0 && s <= totalLength ); //Check that requested arc length is within valid range.
    
    int front = 0;                              //First index.
    int back = (int)table->size() - 1;          //Last index.
    int middle = floor( (front + back) / 2.0 ); //Position to check for searched value.
    
    while( front != middle )                //Search until there are only two locations to look at.
    {
        if( table->at( middle )->second == s )  //Did it find it?
            break;
        
        if( s < table->at( middle )->second )
            back = middle;                  //Narrow search space to the left range.
        else
            front = middle;                 //Narrow search space to the right range.
        
        middle = floor( (front + back) / 2.0 );
    }
    
    //Linearly interpolate known u values to obtain final u parameter.
    double scaling = ( s - table->at( middle )->second ) / ( table->at( middle+1 )->second - table->at( middle )->second );
    return table->at( middle )->first + scaling * ( table->at( middle+1 )->first - table->at( middle )->first );
}


