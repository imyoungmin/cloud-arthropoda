#include "ADrawing.h"

/***************************************************************************************************
Function to draw a string label.
@s - a const pointer to the string to show.
@size - the font size.
***************************************************************************************************/
void ADrawing::drawLabel( const char *s, int size )
{
	int len = (int)strlen( s );
    void *glutbitmap;
    switch( size )
    {
	case 10: glutbitmap = GLUT_BITMAP_HELVETICA_10; break;
	case 12: glutbitmap = GLUT_BITMAP_HELVETICA_12; break; 
	case 18: glutbitmap = GLUT_BITMAP_HELVETICA_18; break;
	default: glutbitmap = GLUT_BITMAP_HELVETICA_12; break;
    }
	
    for( int I = 0; I < len; I++ )			//Render character by character.
      glutBitmapCharacter( glutbitmap, s[I] );
}

/***************************************************************************************************
Function to render a solid cylinder oriented along the Z axis. 
Both bases are of radius 1. 
The bases of the cylinder are placed at Z = 0, and at Z = 1.
***************************************************************************************************/
void ADrawing::drawCylinder()
{
	static GLUquadricObj *cyl = NULL;
	if( cyl == NULL )
	{
		cyl = gluNewQuadric();
	}
	if( cyl == NULL )
	{
		return;
	}
	gluQuadricDrawStyle( cyl, GLU_FILL );
	gluQuadricNormals( cyl, GLU_SMOOTH );
	gluCylinder(cyl, 1.0, 1.0, 1.0, 10, 10);
}

/***************************************************************************************************
Function to render a solid cone oriented along the Z axis with base of radius 1.
The base of the cone is placed at Z = 0, and the appex at Z = 1. 
***************************************************************************************************/
void ADrawing::drawCone()
{
	glutSolidCone( 1, 1, 20, 20 );
}

/***************************************************************************************************
Function that draws a tesselated square.
***************************************************************************************************/
void ADrawing::drawSquareTex()
{
	float i, j;

	int NSUB = 50;
	float d = 1.0 / NSUB;
	
	for( i = -0.5; i < 0.5; i += d )
		for( j = -0.5; j < 0.5; j += d )
		{
			glBegin( GL_POLYGON );
			glNormal3f( 0, 0, 1 );
			float r = 1.0;
			r = 0.5 + cos( ( i*i+j*j ) * 20 * PI );
			setColor( r, 0.5, 0.1 );
			glVertex3f( i, j, 0);
			glVertex3f( i+d, j, 0);
			glVertex3f( i+d, j+d, 0);
			glVertex3f( i, j+d, 0);
			glEnd();
		}
}

/***************************************************************************************************
Function that draws a cube with dimensions 1,1,1 centered at the origin.
***************************************************************************************************/
void ADrawing::drawCube()
{
	glutSolidCube( 1.0 );
}

/***************************************************************************************************
Function that draws a sphere with radius 1 centered around the origin.
***************************************************************************************************/
void ADrawing::drawSphere()
{
	glutSolidSphere( 1.0, 50, 50 );
}

/***************************************************************************************************
Function that sets all material properties to the given RGB color.
***************************************************************************************************/
void ADrawing::setColor(float r, float g, float b)
{
	float ambient = 0.2f;
	float diffuse = 0.5f;
	float specular = 0.9f;
	GLfloat mat[4];
      
	/**** Set ambient lighting parameters ****/
    mat[0] = ambient * r;
    mat[1] = ambient * g;
    mat[2] = ambient * b;
    mat[3] = 1.0;
    glMaterialfv( GL_FRONT_AND_BACK, GL_AMBIENT, mat );

    /**** Set diffuse lighting parameters ******/
    mat[0] = diffuse * r;
    mat[1] = diffuse * g;
    mat[2] = diffuse * b;
    mat[3] = 1.0;
    glMaterialfv( GL_FRONT_AND_BACK, GL_DIFFUSE, mat );

    /**** Set specular lighting parameters *****/
    mat[0] = specular * r;
    mat[1] = specular * g;
    mat[2] = specular * b;
    mat[3] = 1.0;
    glMaterialfv( GL_FRONT_AND_BACK, GL_SPECULAR, mat );
    glMaterialf( GL_FRONT_AND_BACK, GL_SHININESS, 1.0 );
}

/***************************************************************************************************
 Function to transform an ODE orientation matrix into an OpenGL transformation
 matrix (orientation plus position).
 p -> a vector of three elements.
 R -> the rotation matrix, in row vector format, with 12 elements.
 M -> the resulting matrix, in vector format, with 16 elements.
***************************************************************************************************/
void ADrawing::ODEToOpenGLMatrix( const double* p, const double* R, double* M )
{
	M[0] = R[0];  M[4] = R[1];  M[8]  = R[2];   M[12] = p[0];
    M[1] = R[4];  M[5] = R[5];  M[9]  = R[6];   M[13] = p[1];
    M[2] = R[8];  M[6] = R[9];  M[10] = R[10];  M[14] = p[2];
    M[3] = 0;     M[7] = 0;     M[11] = 0;      M[15] = 1;
}

/***************************************************************************************************
 Function to render a box, given it sides length, position and orientation.
***************************************************************************************************/
void ADrawing::odeRenderBox( const double sides[3], const double position[3], const double orientation[12] )
{
	glPushMatrix();					//Save current ModelView.
	
	double Matrix[16];				//The OpenGL version of the transformation matrix.
	ODEToOpenGLMatrix( position, orientation, Matrix );
	glMultMatrixd( Matrix );
	glScaled( sides[0], sides[1], sides[2] );	//Scale to have the right measure in sides.
	setColor( 0.5, 0.6, 0.7 );
	drawCube();
    
	glPopMatrix();					//Restore ModelView.
}

/***************************************************************************************************
 Function to render a sphere, given its radius, position and orientation.
***************************************************************************************************/
void ADrawing::odeRenderSphere( const double radius, const double position[3], const double orientation[12] )
{
	glPushMatrix();					//Save current ModelView.
    
	double Matrix[16];				//OpenGL version of the transormation matrix.
	ODEToOpenGLMatrix( position, orientation, Matrix );
	glMultMatrixd( Matrix );
	glScaled( radius, radius, radius );		//Scale to the sphere radius.
	setColor( 0.0, 1.0, 0.0 );
	drawSphere();
    
	glPopMatrix();
}

/***************************************************************************************************
 Function to render a capsule, given its radius, length, position and orientation.
***************************************************************************************************/
void ADrawing::odeRenderCapsule( const double radius, const double length, const double position[3], const double orientation[12] )
{
	glPushMatrix();					//Save current ModelView.
    
	double Matrix[16];				//OpenGL equivalent version of ODE orientation.
	ODEToOpenGLMatrix( position, orientation, Matrix );
	glMultMatrixd( Matrix );
    
	glPushMatrix();					//Draw cylinder.
	glScaled( radius, radius, length );
	glTranslated( 0.0, 0.0, -0.5 );
	drawCylinder();
	glPopMatrix();
    
	glTranslated( 0.0, 0.0, length/2.0 );	//Draw first cap.
	glPushMatrix();
	glScaled( radius, radius, radius );
	drawSphere();
	glPopMatrix();
    
	glTranslated( 0.0, 0.0, -length );		//Draw second cap.
	glScaled( radius, radius, radius );
	drawSphere();
    
	glPopMatrix();
}

/***************************************************************************************************
 Function to draw a geometry object.
 ***************************************************************************************************/
void ADrawing::odeDrawGeom( dGeomID g, const double *position, const double *orientation )
{
	if( !g )		//If the geometry object is missing, end the function.
		return;
    
	if( !position )	//Position was not passed?
		position = dGeomGetPosition( g );		//Then, get the geometry position.
    
	if( !orientation )	//Orientation was not given?
		orientation = dGeomGetRotation( g );	//And get existing geometry orientation.
    
	int type = dGeomGetClass( g );				//Get the type of geometry.
	
	if( type == dBoxClass )						//Is it a box?
	{
		double sides[3];
		dGeomBoxGetLengths( g, sides );				//Get length of sides.
		odeRenderBox( sides, position, orientation );	//Render the actual box in environment.
	}
    
	if( type == dSphereClass )					//Is it a sphere?
	{
		double radius;
		radius = dGeomSphereGetRadius( g );				//Get the radius.
		odeRenderSphere( radius, position, orientation );	//Render sphere in environment.
	}
    
	if( type == dCapsuleClass )
	{
		double radius;
		double length;
		dGeomCapsuleGetParams( g, &radius, &length );	//Get both radius and length.
		odeRenderCapsule( radius, length, position, orientation );	//Render capsule in environment.
	}
    
	if( type == dGeomTransformClass )					//Is it an embeded geom in a composite body.
	{
		dGeomID g2 = dGeomTransformGetGeom( g );		//Get the actual geometry inside the wrapper.
		const double *position2 = dGeomGetPosition( g2 );	//Get position and orientation of wrapped geometry.
		const double *orientation2 = dGeomGetRotation( g2 );
		
		dVector3 actualPosition;						//Real world coordinated position and orientation
		dMatrix3 actualOrientation;						//of the wrapped geometry.
		
		dMultiply0_331( actualPosition, orientation, position2 );	//Get world coordinates of geometry position.
		actualPosition[0] += position[0];
		actualPosition[1] += position[1];
		actualPosition[2] += position[2];
        
		dMultiply0_333( actualOrientation, orientation, orientation2 );	//Get world coordinates of geom orientation.
        
		odeDrawGeom( g2, actualPosition, actualOrientation );	//Draw embeded geometry.
	}
}



