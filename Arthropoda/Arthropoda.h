//
//  Arthropoda.h
//  Arthropoda
//
//  Created by Luis Angel on 9/16/14.
//  Copyright (c) 2014 Luis Angel. All rights reserved.
//

#ifndef __Arthropoda__Arthropoda__
#define __Arthropoda__Arthropoda__

#include <assert.h>
#include "Ball.h"
#include "FrameSaver.h"
#include "Timer.h"
#include "AMath.h"
#include "ADrawing.h"
#include "AOde.h"

using namespace AMath;

//Declaration of main loop in OpenGL because it is necessary in ATcl.
void openGLMainLoop();

class Arthropoda
{
private:
    //OpenGL global variables.
	static Timer globalTimer;
	static FrameSaver *frameSaver;					//FrameSaver object to record animation.
	static BallData *arcBall;						//For mouse manipulation.
	static int mouseButton;							//Which mouse button is pressed.
	static float zoom;								//Zoom on viewport.
	static int previousY;							//
	static bool simulate;							//Simulation flag.
	static bool step;								//One simulation step.
	static bool record;								//Recording flag.
	static AVector3D eye;                           //OpenGL viewer eye location.
	static AVector3D reference;                     //OpenGL reference point.
	static double simulationTime;					//Simulation time.
	static double simulationStep;					//Simulation time step.
    static int viewportWidth;						//Viewport width.
	static int viewportHeight;						//Viewport height.

    static void render();
	static void restoreView();
	static void resetTime();
	static void reshapeView( int width, int height );
	static void mousePressed( int button, int state, int x, int y );
	static void mouseMoved( int x, int y );
	static void keyPressed( unsigned char key, int x, int y );
    static void specialKeyPressed( int key, int xx, int yy );
	static void resetArcBall();
	static void initOpenGl();
	static void plotInstructions();
	static void idle();
    
public:
    //ODE variables.
    static AOde* ode;
    
	//OpenGL callback functions.
    static int launchApplication( int argc, char* argv[] );
};

#endif /* defined(__Arthropoda__Arthropoda__) */
