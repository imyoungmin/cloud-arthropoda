/**********************************************************************************
Class with drawing methods for OpenGL
**********************************************************************************/
#pragma once

#include <math.h>
#include "GL/freeglut.h"
#include "GL/glut.h"
#include "AMath.h"
#include "ode/ode.h"

using namespace AMath;

class ADrawing
{
public:
    
    //OpenGL general drawing functions.
	static void drawLabel( const char *s, int size );
	static void drawCylinder();
	static void drawCone();
	static void drawSquareTex();
	static void drawSphere();
	static void drawCube();
	static void setColor( float r, float g, float b );
    
    //ODE drawing functions.
private:
    static void ODEToOpenGLMatrix( const double* p, const double* R, double* M );
    static void odeRenderBox( const double sides[3], const double position[3], const double orientation[12] );
    static void odeRenderSphere( const double radius, const double position[3], const double orientation[12] );
    static void odeRenderCapsule(const double radius, const double length, const double position[3], const double orientation[12]);
public:
    static void odeDrawGeom( dGeomID g, const double *position, const double *orientation );
};

