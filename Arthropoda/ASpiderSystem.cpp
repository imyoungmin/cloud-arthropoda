//
//  ASpiderSystem.cpp
//  Arthropoda
//
//  Created by Luis Angel on 9/22/14.
//  Copyright (c) 2014 Luis Angel. All rights reserved.
//

#include "ASpiderSystem.h"


/**
 * @brief Spider system constructor.
 * @param w ODE world pointer.
 * @param s ODE space pointer.
 * @param deltaT ODE time step.
 */
ASpiderSystem::ASpiderSystem( dWorldID w, dSpaceID s, double deltaT )
{
    assert( w );                                                // Check that world and space have been already created.
    assert( s );
    assert( deltaT > 0 );
    
    // Assign copies of ODE world, space, and simualtion time step.
    world = w;
    space = s;
    timeStep = deltaT;
    
    // Initializing objects.
    objects = vector< vector< AObject > >( LEG_COUNT + 1 );     // Take into account the trunk, too.
    objects[TRUNK] = vector< AObject >( 2 );                    // Trunk has two links: opisthosoma and prosoma.
	for( int leg = L_LEG1; leg <= LEG_COUNT; leg++ )
        objects[leg] = vector< AObject >( 4 );                  // Each leg has 4 independent links.
    
    // Initializing collision codes.
    collisionCodes = vector< vector< unsigned char > >( LEG_COUNT + 1 );    // Take into account to trunk too.
    collisionCodes[TRUNK] = vector< unsigned char >( 2 );                   // Trunk has two (grouped) members.
    collisionCodes[TRUNK][OPISTHOSOMA]  = encodeCollisionCode( TRUNK, OPISTHOSOMA );
    collisionCodes[TRUNK][PROSOMA]      = encodeCollisionCode( TRUNK, PROSOMA );
    
    for( int leg = L_LEG1; leg <= LEG_COUNT; leg++ )
    {
        collisionCodes[leg] = vector< unsigned char >( 4 );                 // Each leg has 4 geometries subject to collision.
        collisionCodes[leg][FEMUR]      = encodeCollisionCode( leg, FEMUR );
        collisionCodes[leg][TIBIA]      = encodeCollisionCode( leg, TIBIA );
        collisionCodes[leg][METATARSUS] = encodeCollisionCode( leg, METATARSUS );
        collisionCodes[leg][TARSUS]     = encodeCollisionCode( leg, TARSUS );
    }
    
    // Initializing Inverse Kinematics struct vector.
    ikBodyParts = vector< IKStruct >( LEG_COUNT + 1 );          // Take into account the trunk, too.
    for( int I = 0; I <= LEG_COUNT; I++ )
    {
        ikBodyParts[I].ikTime = 0.0;                            // Initialize IK variables.
        ikBodyParts[I].fMax[AXIS_1] = 0.0;                      // No motor enabled, yet, in any of the joints.
        ikBodyParts[I].fMax[AXIS_2] = 0.0;
        ikBodyParts[I].state = S_INIT;                          // Part is being initialized.
        ikBodyParts[I].oldCumulativeVelocity = 0.0;             // Initialize cumulative velocity.
        ikBodyParts[I].newCumulativeVelocity = 0.0;
		ikBodyParts[I].traveledDistance = 0.0;					// Initialize traveled distance along a parametric curve.
        ikBodyParts[I].minimumDistanceToStop = 0.0;             // We can test if a body part has stop only after traversing this.
    }
    
	// Initializing joints.
	joints = vector< vector< AJoint > >( LEG_COUNT + 1 );
    
	// Initializing the joints group INHERITED from ABaseSystem class.
	jointGroup = dJointGroupCreate( 0 );                        // Zero to indicate undefined size (and backward compatibility).
    
	// Define the location of the body root.
    double prosomaLocationArray[] = { 0.015, 0.2, 0.0 };
	prosomaLocation = AVector3D( prosomaLocationArray );
    
	// Init opisthosoma.
	initOpisthosoma();
	
	// We define here what we need for legs too.
	double prosomaRadii[12] = { 0.005, 0.003, 0.003, 0.003, 0.003, 0.002, 0.003, 0.003, 0.003, 0.003, 0.005, 0.002 };
	double prosomaLengths[12] = { 0.01, 0.014, 0.0115, 0.0115, 0.009, 0.0085, 0.009, 0.0115, 0.0115, 0.014, 0.01, 0.0085 };
	
    // Init prosoma and pedicel.
    initProsoma( prosomaRadii, prosomaLengths );
    
	// Init legs, but first, define joint radius for drawing and spacing links along legs.
	const double jointRadius[] = { legLinkRadii[FEMUR],					// Radii for joints is average of radius of the two joined links.
								   ( legLinkRadii[FEMUR] + legLinkRadii[TIBIA] ) / 2.0,
								   ( legLinkRadii[TIBIA] + legLinkRadii[METATARSUS] ) / 2.0,
								   ( legLinkRadii[METATARSUS] + legLinkRadii[TARSUS] ) / 2.0,
								   0.0 };								// Claw radius is zero.
	const double factorL[] = { 0, 1, 2, 5, 6, 13, 12, 9, 8 };
	const double factorX[] = { 0, 2, 3, 4, 5, 10, 9, 8, 7 };
	
	for( int L = L_LEG1; L <= LEG_COUNT; L++ )							// Generate leg one by one, from left 1, to right 4.
		initLeg( L, factorL[L]*PI/7.0 + HALF_PI, factorX[L]*PI/6.0, prosomaRadii[L%4+1], prosomaLengths[L%4+1], jointRadius );

    //////////////////////////////// Initialize control constants //////////////////////////////////
	
    showProfileSpheres	= true;
	beganFalling		= false;
	showHermiteCurves	= true;
	walk				= 0;		// Walking sequence flag.
	walkDelay			= 20;		// Time steps to start next swing leng.
	nLegsStepping		= 0;
    
    ///////////////////////////////// Initialize Spider leg motion /////////////////////////////////
	
	setSpiderProfile( 0.0 );
    for( int L = L_LEG1; L <= LEG_COUNT; L++ )
    {
        ikBodyParts[L].fMax[AXIS_1] = MAX_JOINT_FORCE;					// Give force to legs, so that they begin
        ikBodyParts[L].fMax[AXIS_2] = MAX_JOINT_FORCE;					// at a desired angle.
    }

	//////////////////////////////// Initialize stepping sequence //////////////////////////////////
	
	//                      +-------- Tetrapod 1 --------+  +-------- Tetrapod 2 --------+
	int legs[LEG_COUNT] = { L_LEG1, R_LEG2, L_LEG3, R_LEG4, R_LEG1, L_LEG2, R_LEG3, L_LEG4 };
	for( int L = 0; L < LEG_COUNT; L++ )
		steppingSequence.push_back( pair< int, int >( legs[L], 0 ) );
	
	rFollowOld = rFollow = zeroVector3D;								// Used to compute velocity r^dot.
	rHeightOld = rHeight = 0;											// Used for height regulation.
}


/**
 * @brief Initializes opisthosoma in spider body.
 */
void ASpiderSystem::initOpisthosoma()
{
	/////////////// Initializing the Opisthosoma skeleton /////////////////
    
	objects[TRUNK][OPISTHOSOMA] = AObject();
	objects[TRUNK][OPISTHOSOMA].geoms = vector< dGeomID >( 3 );					// With three geometries.
    
	objects[TRUNK][OPISTHOSOMA].body = dBodyCreate( world );					// Create a new body and attach it to object.
	dBodySetPosition( objects[TRUNK][OPISTHOSOMA].body,
                     prosomaLocation[X] - 0.0345,
                     prosomaLocation[Y] + 0.025,
                     prosomaLocation[Z] );
    
	dMass m, opisthosomaM;									//Start accumulating mass.
	dMassSetZero( &opisthosomaM );
	double linkMass = opisthosomaMass / (double)objects[TRUNK][OPISTHOSOMA].geoms.size();
	
	// Positions for the three geometries.
	//											X        Y	    Z
	double opisthosomaPositions[3][3] = { {  0.0102 , 0.02165, 0.0 },
										  { -0.00485, 0.03247, 0.0 },
										  { -0.05536, 0.01082, 0.0 } };
    
	const double opisthosomaRadii[3] = { 0.00445, 0.00445, 0.002225 };			// Radii are distinct.
	const double opisthosomaLengths[3] = { 0.03895491, 0.04605, 0.05045 };		// Lengths are distinct.

	dMatrix3 opisthosomaRotations[3];											// Rotations for geoms.
	dRFromEulerAngles( opisthosomaRotations[0], HALF_PI, -0.440287, 0.0 );
	dRFromEulerAngles( opisthosomaRotations[1], HALF_PI, -1.165823, 0.0 );
	dRFromEulerAngles( opisthosomaRotations[2], HALF_PI, -1.165823, 0.0 );
    
	for( int I = 0; I < objects[TRUNK][OPISTHOSOMA].geoms.size(); I++ )			// Iterate over the geoms.
	{
		//Create the geometry.
		objects[TRUNK][OPISTHOSOMA].geoms[I] = dCreateCapsule( space, opisthosomaRadii[I], opisthosomaLengths[I] );
		
		//Create mass distribution for a capsule.
		dMassSetCapsuleTotal( &m, linkMass, 3, opisthosomaRadii[I], opisthosomaLengths[I] );
        
		//Rotate and translate the capsule-mass distribution in the body frame.
		dMassRotate( &m, opisthosomaRotations[I] );
		dMassTranslate( &m, opisthosomaPositions[I][0], opisthosomaPositions[I][1], opisthosomaPositions[I][2] );
        
		//Add geometry mass distribution to body.
		dMassAdd( &opisthosomaM, &m );
	}
    
	for( int I = 0; I < objects[TRUNK][OPISTHOSOMA].geoms.size(); I++)			// Move geoms inside the body.
	{
		//Attach geometry to body.
		dGeomSetBody( objects[TRUNK][OPISTHOSOMA].geoms[I], objects[TRUNK][OPISTHOSOMA].body );
        
		//Set position and orientation for the geoms.
		dGeomSetOffsetRotation( objects[TRUNK][OPISTHOSOMA].geoms[I], opisthosomaRotations[I] );
		dGeomSetOffsetPosition( objects[TRUNK][OPISTHOSOMA].geoms[I],
                               opisthosomaPositions[I][X] - opisthosomaM.c[X],	// Subtract center of mass location since
                               opisthosomaPositions[I][Y] - opisthosomaM.c[Y],	// it became origin of body.
                               opisthosomaPositions[I][Z] - opisthosomaM.c[Z] );
        
        //In order to know when this geometry is in collision, we store its code.
        dGeomSetData( objects[TRUNK][OPISTHOSOMA].geoms[I], &collisionCodes[TRUNK][OPISTHOSOMA] );
	}
    
	//Move center of mass and apply it to body. This makes center of mass to be at the origin of the body frame.
	dMassTranslate( &opisthosomaM, -opisthosomaM.c[X], -opisthosomaM.c[Y], -opisthosomaM.c[Z] );
	dBodySetMass( objects[TRUNK][OPISTHOSOMA].body, &opisthosomaM );
    
	//Data associated with this object.
	dBodySetData( objects[TRUNK][OPISTHOSOMA].body, (void *)0 );
}


/**
 * @brief Initializes prosoma and pedicel joint with opisthosoma.
 * @param prosomaRadii array of prosoma links radii.
 * @param prosomaLengths array of prosoma links lengths.
 */
void ASpiderSystem::initProsoma( const double* prosomaRadii, const double* prosomaLengths )
{
	////////////////////////////// Initializing the prosoma skeleton ///////////////////////////////
    
	objects[TRUNK][PROSOMA] = AObject();
	objects[TRUNK][PROSOMA].geoms = vector< dGeomID >( 12 );                                        // With twelve geometries.
    
	objects[TRUNK][PROSOMA].body = dBodyCreate( world );                                            // Create a new body.
	dBodySetPosition( objects[TRUNK][PROSOMA].body, prosomaLocation[X], prosomaLocation[Y], prosomaLocation[Z] );
	dBodySetLinearVel( objects[TRUNK][PROSOMA].body, 0.0, 0.0, 0.0 );                               // Object begins stationary.
    
	dMass m, prosomaM;                                                                              // Start accumulating mass.
	dMassSetZero( &prosomaM );
	double linkMass = prosomaMass / (double)objects[TRUNK][PROSOMA].geoms.size();
	
	// Positions of the twelve links in the prosoma.
	//                                           X        Y       Z
	const double prosomaPositions[12][3] = { {  0.02   , 0.0, -0.005   },			// First link.
											 {  0.005  , 0.0, -0.00866 },			// Second.
											 {  0.0    , 0.0, -0.00875 },			// Third.
											 { -0.00438, 0.0, -0.00758 },			// ...
											 { -0.00649, 0.0, -0.00375 },
											 { -0.00625, 0.0,  0.0     },
											 { -0.00649, 0.0,  0.00375 },
											 { -0.00438, 0.0,  0.00758 },
											 {  0.0    , 0.0,  0.00875 },
											 {  0.005  , 0.0,  0.00866 },
											 {  0.02   , 0.0,  0.005   },
											 {  0.00625, 0.0,  0.0     } };			// 12th link.
	
	dMatrix3 prosomaRotations[12];                                                                  // Rotations for geoms.
    
	double angle = 0.523599;
	const double prosomaYAngles[] = { 0.0, angle*2.0, angle*3.0, angle*4.0, angle*5.0, 0.0, angle*7.0, angle*8.0, angle*9.0, angle*10.0, 0.0, 0.0 };
    
	for( int I = 0; I < objects[TRUNK][PROSOMA].geoms.size(); I++ )                                 //Iterate over the geoms.
	{
		// Create the geometry.
		objects[TRUNK][PROSOMA].geoms[I] = dCreateCapsule( space, prosomaRadii[I], prosomaLengths[I] );
		
		// Create mass distribution for a capsule.
		dMassSetCapsuleTotal( &m, linkMass, 3, prosomaRadii[I], prosomaLengths[I] );
        
		// Rotate and translate the capsule-mass distribution in the body frame.
		dRFromAxisAndAngle( prosomaRotations[I], 0.0, 1.0, 0.0, prosomaYAngles[I] + HALF_PI );// Create a rotation matrix.
		dMassRotate( &m, prosomaRotations[I] );
		dMassTranslate( &m, prosomaPositions[I][X], prosomaPositions[I][Y], prosomaPositions[I][Z] );
        
		// Add geometry mass distribution to body.
		dMassAdd( &prosomaM, &m );
	}
    
	for( int I = 0; I < objects[TRUNK][PROSOMA].geoms.size(); I++)                                  // Move geoms inside the body.
	{
		// Attach geometry to body.
		dGeomSetBody( objects[TRUNK][PROSOMA].geoms[I], objects[TRUNK][PROSOMA].body );
        
		// Set position and orientation for the geoms.
		dGeomSetOffsetRotation( objects[TRUNK][PROSOMA].geoms[I], prosomaRotations[I] );
		dGeomSetOffsetPosition( objects[TRUNK][PROSOMA].geoms[I],
                               prosomaPositions[I][X] - prosomaM.c[X],                              // Subtract center of mass
                               prosomaPositions[I][Y] - prosomaM.c[Y],                              // location because it became
                               prosomaPositions[I][Z] - prosomaM.c[Z] );                            // origin of body.
        
        // Set data to identify these geoms when they collide.
        dGeomSetData( objects[TRUNK][PROSOMA].geoms[I], &collisionCodes[TRUNK][PROSOMA] );
	}
    
	// Move center of mass and apply it to body. This makes center of mass to be at the origin of the body frame.
	dMassTranslate( &prosomaM, -prosomaM.c[X], -prosomaM.c[Y], -prosomaM.c[Z] );
	dBodySetMass( objects[TRUNK][PROSOMA].body, &prosomaM );
    
	// Data associated with this object.
	dBodySetData( objects[TRUNK][PROSOMA].body, (void *)0 );
    
	/////////////////////////////////// Initializing trunk joints //////////////////////////////////
    
	joints[TRUNK] = vector< AJoint >( 1 );                                                          // Only the pedicel for trunk.
	joints[TRUNK][PEDICEL] = AJoint();
	joints[TRUNK][PEDICEL].joint = dJointCreateHinge( world, jointGroup );
	dJointAttach( joints[TRUNK][PEDICEL].joint, objects[TRUNK][OPISTHOSOMA].body, objects[TRUNK][PROSOMA].body );
    double anchorArray[] = { prosomaLocation[X]-0.015, prosomaLocation[Y]+0.00225, prosomaLocation[Z] };
	joints[TRUNK][PEDICEL].anchor = AVector3D( anchorArray );
	dJointSetHingeAnchor( joints[TRUNK][PEDICEL].joint,
                         joints[TRUNK][PEDICEL].anchor[X], joints[TRUNK][PEDICEL].anchor[Y], joints[TRUNK][PEDICEL].anchor[Z] );
	dJointSetHingeAxis( joints[TRUNK][PEDICEL].joint, 0.0, 0.0, 1.0 );
	dJointSetHingeParam( joints[TRUNK][PEDICEL].joint, dParamHiStop, 0.0 );     // By setting limits to zero we make sure
	dJointSetHingeParam( joints[TRUNK][PEDICEL].joint, dParamLoStop, 0.0 );     // that the hinge will try to stay there.
	
    double jointMass = prosomaMass + opisthosomaMass;
    double kp = 3.0 * jointMass;                        // Spring constant.
    double kd = 2.0 * sqrt( jointMass * kp );           // Damping constant.
    double erp = timeStep*kp / ( timeStep*kp + kd );                  // Joint stop ERP and CFM.
    double cfm = 1.0 / ( timeStep*kp + kd );
    dJointSetHingeParam( joints[TRUNK][PEDICEL].joint, dParamStopERP, erp );
    dJointSetHingeParam( joints[TRUNK][PEDICEL].joint, dParamStopCFM, cfm );
    
    joints[TRUNK][PEDICEL].radius = 0.002;
    
    // Anchoring spider to some point above the surface.
//	dJointID prosomaFixer = dJointCreateFixed( world, jointGroup );
//	dJointAttach( prosomaFixer, objects[TRUNK][PROSOMA].body, NULL );      // Attach prosoma to world.
//	dJointSetFixed( prosomaFixer );
	
}


/**
 * @brief Inits a leg and its joints.
 * @param LL leg index: from L_LEG1 to R_LEG4.
 * @param angleL angle for full leg rotation (departing from +z axis).
 * @param angleX angle with respect to +x axis for location of leg beginning.
 * @param prosomaLinkRadius radius of prosoma link to which this leg is attached.
 * @param prosomaLinkLength length of prosoma link to which this leg is attached.
 * @param jointRadius array of joint radii used for drawing and spacing in joints calculation.
 */
void ASpiderSystem::initLeg( int LL, double angleL, double angleX, double prosomaLinkRadius, double prosomaLinkLength, const double* jointRadius )
{
	dMatrix3 rot;
	const double yAxis[] = { 0.0, 1.0, 0.0 };
	
	dRFromAxisAndAngle( rot, yAxis[X], yAxis[Y], yAxis[Z], angleL );	// Same rotation for all links along this leg.
	double pos[3] = { ( 2.0*prosomaLinkRadius + prosomaLinkLength )*cos( angleX ) + prosomaLocation[X] - 0.002,
		prosomaLocation[Y],
		- ( 2.0*prosomaLinkRadius + prosomaLinkLength )*sin( angleX ) - prosomaLocation[Z] };	//Root (start) of leg.
	
	dMass lm[4];
	double r = 0.0;														// Cummulative length for leg.
	
	double jointAnchorPositions[4][3];									// 3D positions for joint anchors.
	double orthogonalAxis[] = { cos( angleL ), 0.0, -sin( angleL ) };	// Stores unit axis perpendicular to leg axis and Y-axis.
    
	legsComfortAngles[LL] = angleL;										// Angle departs from spider z axis.
	
	for( int link = FEMUR; link <= TARSUS; link++ )
	{
		jointAnchorPositions[link][X] = r*sin( angleL ) + pos[X];		// Anchor position for joint before this link.
		jointAnchorPositions[link][Y] = pos[Y];
		jointAnchorPositions[link][Z] = r*cos( angleL ) + pos[Z];
		
		objects[LL][link] = AObject();									// Create new object for link.
		objects[LL][link].geoms = vector< dGeomID >( 1 );				// Only one geometry: a capsule.
		objects[LL][link].body = dBodyCreate( world );					// Create a body for link object.
		
		double linkLength = 0.0;										// Holds cylinder length of link capsule (no caps).
		switch( link )													// Select appropriate link length.
		{
			case FEMUR: linkLength = femurLengths[LL]; break;
			case TIBIA: linkLength = tibiaLengths[LL]; break;
			case METATARSUS: linkLength = metatarsusLengths[LL]; break;
			case TARSUS: linkLength = tarsusLengths[LL];
		}
		double length = linkLength + 2.0 * legLinkRadii[link];			// Holds the total length of link capsule.
		
		double nextJointRadius = 0.0;									// Joint radius of following link.
		if( link + 1 <= TARSUS )										// Between limits?
			nextJointRadius = jointRadius[link+1];
		
		dBodySetPosition( objects[LL][link].body,						// Add translation with space for joint.
						 ( r + ( jointRadius[link]+length-nextJointRadius )/2.0 ) * sin( angleL ) + pos[X],
						 pos[Y],
						 ( r + ( jointRadius[link]+length-nextJointRadius )/2.0 ) * cos( angleL ) + pos[Z] );
		dBodySetRotation( objects[LL][link].body, rot );				// Rotate link.
		
		double effectiveLinkLength = linkLength - jointRadius[link] - nextJointRadius;
		
		dMassSetCapsuleTotal( &lm[link], legLinkMass[link], 3, legLinkRadii[link], effectiveLinkLength );	// Capsule mass.
		
		objects[LL][link].geoms[0] = dCreateCapsule( space, legLinkRadii[link], effectiveLinkLength );		// Capsule geom.
		
		dGeomSetBody( objects[LL][link].geoms[0], objects[LL][link].body );				// Link bodies and geoms.
		dGeomSetData( objects[LL][link].geoms[0], &collisionCodes[LL][link] );			// Identify geom for collision detection.
		dBodySetMass( objects[LL][link].body, &lm[link] );								// Link mass to body.
		
		r += length;													// Advance to end of current link.
		
	}
    
	///////////////////////////////// Preparing joints for this leg ////////////////////////////////
    
	// Joints between links along this leg.
	joints[LL] = vector< AJoint >( 5 );									// 5 joints (including claw at the end).
	
	for( int J = COXA; J <= ANKLE; J++ )
	{
		joints[LL][J] = AJoint();										// Create a new joint object.
		joints[LL][J].joint = dJointCreateUniversal( world, jointGroup );
		
		// Define first and second bodies to attach to this joint.
		dBodyID body1 = NULL;
		dBodyID body2 = NULL;
		if( J == COXA )
		{
			body1 = objects[LL][FEMUR].body;							// Only coxa has as previous link the prosoma.
			body2 = objects[TRUNK][PROSOMA].body;
		}
		else
		{	body1 = objects[LL][J].body;								// Rest of joints have links within the leg.
			body2 = objects[LL][J-1].body;								// Except claw, which is created later.
		}
		dJointAttach( joints[LL][J].joint, body1, body2 );				// Attach selected bodies.
		
		joints[LL][J].anchor = AVector3D( jointAnchorPositions[J] );	// Define anchor position.
		dJointSetUniversalAnchor( joints[LL][J].joint, joints[LL][J].anchor[X], joints[LL][J].anchor[Y], joints[LL][J].anchor[Z] );
		
		dJointSetUniversalAxis1( joints[LL][J].joint, orthogonalAxis[0], orthogonalAxis[1], orthogonalAxis[2] );
		dJointSetUniversalAxis2( joints[LL][J].joint, yAxis[X], yAxis[Y], yAxis[Z] );	// Define joint axes.
		
		joints[LL][J].radius = jointRadius[J];							// Joint radius defined in links creation above.
		joints[LL][J].desiredAngle = vector< double >{ 0.0, 0.0 };		// Initial desired angle for both axes.
	}
	
	// The claw joint.
	
	joints[LL][CLAW] = AJoint();
	joints[LL][CLAW].joint = NULL;												// No joint associated, yet.
}

/**
 * @brief Sets spider profile or comfort positions for its legs.
 * @remarks Profile points are given in prosoma coordinates.
 * @param radius a number between -1 and 1, where -1 is the closest to body center, and 1 the farthest.
 */
void ASpiderSystem::setSpiderProfile( double radius )
{
	//Iterate over legs to compute their comfort point.
	for( int I = L_LEG1; I <= LEG_COUNT; I++ )
	{
		dVector3 anchor;
		dJointGetUniversalAnchor2( joints[I][COXA].joint, anchor );		//Joint anchor with respect to attached prosoma link.
		dVector3 prosomaPoint;											//Coordinates in prosoma coordinates.
		dBodyGetPosRelPoint( objects[TRUNK][PROSOMA].body, anchor[X], anchor[Y], anchor[Z], prosomaPoint );
		
		//Get length of full leg.
		double length = 0.0;
		length += femurLengths[I] + legLinkRadii[FEMUR];				//Add femur.
		length += tibiaLengths[I] + legLinkRadii[TIBIA];				//Add tibia.
		length += metatarsusLengths[I] + legLinkRadii[METATARSUS];		//Add metatarsus.
		length += tarsusLengths[I] + legLinkRadii[TARSUS];				//Add tarsus.
        
		//Set profile in prosoma coordinates.
		double r = TWO_THIRDS * length + radius * length * ONE_SIXTH;
        double profileArray[] = { r * sin( legsComfortAngles[I] ) + prosomaPoint[X],
            PROFILE_HEIGHT,
            r * cos( legsComfortAngles[I] ) + prosomaPoint[Z] };
        profile[I] = AVector3D( profileArray );
	}
}


/**
 * @brief Registers user commands for TclTk on the spider system.
 * @param objc number of parameters given as input.
 * @param objv an array that contains each of the parameters passed to the command.
 * @return TCL_ERROR if the command cannot be executed correctly, TCL_OK otherwise.
 */
int ASpiderSystem::command( int objc, Tcl_Obj *CONST objv[] )
{
    if( objc < 1 )
	{
		ATcl::outputMessage( "Wrong number of parameters for spider system" );
		return TCL_ERROR;
	}
	else
    {
        char* command = Tcl_GetString( objv[0] );           //What is the command for this spider system.
        if( strcasecmp( command, "showProfileSpheres" ) == 0 )  //Display or not spheres.
        {
            if( objc == 2 )
            {
                try
                {
                    int show = atoi( Tcl_GetString( objv[1] ) );
                    if( show )
                        showProfileSpheres = true;
                    else
                        showProfileSpheres = false;
                    glutPostRedisplay();                                //Update picture.
                }
                catch( exception& e )
                {
                    ATcl::outputMessage( "Wrong parameter '%s' for command showProfileSpheres", Tcl_GetString( objv[1] ) );
                    return TCL_ERROR;
                }
            }
            else
            {
                ATcl::outputMessage( "Usage: showProfileSpheres <0|1>");
                return TCL_ERROR;
            }
        }
        else
        {
            if( strcasecmp( command, "showHermiteCurves" ) == 0 )   //Display parametric curves?
            {
                if( objc == 2 )
                {
                    try
                    {
                        int show = atoi( Tcl_GetString( objv[1] ) );
                        if( show )
                            showHermiteCurves = true;
                        else
                            showHermiteCurves = false;
                        glutPostRedisplay();
                    }
                    catch( exception& e )
                    {
                        ATcl::outputMessage( "Wrong parameter '%s' for command showHermiteCurves", Tcl_GetString( objv[1] ) );
                        return TCL_ERROR;
                    }
                }
                else
                {
                    ATcl::outputMessage( "Usage: showHermiteCurves <0|1>" );
                    return TCL_ERROR;
                }
            }
            else
            {
				if( strcasecmp( command, "dumpAngles" ) == 0 )		// Show all leg angles?
				{
					if( objc == 2 )
					{
						try
						{
							int leg = atoi( Tcl_GetString( objv[1] ) );
							ATcl::outputMessage( dumpAngles( leg ).c_str() );
						}
						catch( exception& e )
						{
							ATcl::outputMessage( "Wrong parameter '%s' for command dumpAngles", Tcl_GetString( objv[1] ) );
							return TCL_ERROR;
						}
					}
					else
					{
						ATcl::outputMessage( "Usage: dumpAngles <leg_index>" );
						return TCL_ERROR;
					}
				}
				else
				{
					if( strcasecmp( command, "walk" ) == 0 )		// Walk at a given speed?
					{
						if( objc == 2 )
						{
							try
							{
								int speed = atoi( Tcl_GetString( objv[1] ) );
								walk = max( 0, min( speed, 3 ) );
							}
							catch ( exception& e )
							{
								ATcl::outputMessage( "Wrong parameter '%s' for command walk", Tcl_GetString( objv[1] ) );
								return TCL_ERROR;
							}
						}
						else
						{
							ATcl::outputMessage( "Usage: walk <[0-3]>" );
							return TCL_ERROR;
						}
					}
					else
					{
						ATcl::outputMessage( "Unknown option '%s' for spider system", Tcl_GetString( objv[0] ) );
						ATcl::outputMessage( "Try: \n"
											" showProfileSpheres \n"
											" showHermiteCurves \n"
											" dumpAngles \n"
											" walk" );
						return TCL_ERROR;
					}
				}
            }
        }
    }
	return TCL_OK;
}


/**
 * @brief Draws the spider system.
 */
void ASpiderSystem::drawSystem()
{
    /////////////////////////////////////// Drawing links //////////////////////////////////////////
	for( int I = 0; I < objects.size(); I++ )
	{
		if( I < L_LEG1 )		// Differentiate body links from legs links.
			ADrawing::setColor( 0.5294, 0.551, 0.7451 );
		else					// Legs.
		{
			if( ikBodyParts[I].state != S_STANCE )
				ADrawing::setColor( 0.7882, 0.7843, 0.698 );
			else
				ADrawing::setColor( 0.3882, 0.5843, 0.698 );
		}
		
		for( int J = 0; J < objects[I].size(); J++ )
		{
			for( int K = 0; K < objects[I][J].geoms.size(); K++ )
			{
                ADrawing::odeDrawGeom( objects[I][J].geoms[K], NULL, NULL );
			}
		}
	}
    
	////////////////////////////////////// Drawing joints //////////////////////////////////////////
	dVector3 anchor;
	for( int I = 0; I < joints.size(); I++)
	{
		for( int J = 0; J < joints[I].size(); J++ )
		{
			if( joints[I][J].joint )							// Draw joint as long as it is not null.
			{
				switch( dJointGetType( joints[I][J].joint ) )
				{
					case dJointTypeHinge:
						ADrawing::setColor( 0.0, 0.8, 0.8 );			//Hinges.
						dJointGetHingeAnchor( joints[I][J].joint, anchor );
						break;
					case dJointTypeUniversal:
					default:
						ADrawing::setColor( 0.8902, 0.7804, 0.298 );	//Universal joints.
						dJointGetUniversalAnchor( joints[I][J].joint, anchor );
						break;
				}
				
				glPushMatrix();		//Do the actual drawing.
				glTranslatef( anchor[X], anchor[Y], anchor[Z] );
				double scale = ( joints[I][J].radius )? joints[I][J].radius: 0.0015;
				glScalef( scale, scale, scale );
				ADrawing::drawSphere();
				glPopMatrix();
			}
		}
	}
    
    //////////////////////////////////// Draw parametric curves ////////////////////////////////////
    if( showHermiteCurves )
    {
        for( int I = L_LEG1; I <= LEG_COUNT; I++ )		// Only for legs.
		{
			const double* R = dBodyGetRotation( objects[TRUNK][PROSOMA].body );
			const double* T = dBodyGetPosition( objects[TRUNK][PROSOMA].body );
			
            ikBodyParts[I].hermiteCurve.drawCurve( R, T );
		}
    }
    
	//////////////////////////////////// Draw the comfort spheres //////////////////////////////////
    if( showProfileSpheres )
    {
        glDisable( GL_LIGHTING );
        glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
        glEnable( GL_BLEND );
        glColor4f( 1.0, 1.0, 0.0, 0.2 );
        for( int I = L_LEG1; I <= LEG_COUNT; I++ )
        {
            dVector3 point;
            glPushMatrix();
            dBodyGetRelPointPos( objects[TRUNK][PROSOMA].body, profile[I][X], profile[I][Y], profile[I][Z], point );
            glTranslated( point[X], point[Y], point[Z] );
            glScaled( profileSphereRadius, profileSphereRadius, profileSphereRadius );
            ADrawing::drawSphere();
            glPopMatrix();
        }
        glDisable( GL_BLEND );
        glEnable( GL_LIGHTING );
    }
	
	/////////////////////////////////// Draw the rFollow vector ////////////////////////////////////
	
	dVector3 rfw;
	dBodyVectorToWorld( objects[TRUNK][PROSOMA].body, rFollow[X], rFollow[Y], rFollow[Z], rfw );
	dVector3 pw;
	dBodyGetRelPointPos( objects[TRUNK][PROSOMA].body, -0.000479276, -0.02, 0, pw );
	AVector3D Pw( pw );		// Prosoma center.
	AVector3D to = Pw + AVector3D( rfw );
	
	glPushMatrix();
	glTranslated( Pw[X], Pw[Y], Pw[Z] );
	glScaled( 0.0015, 0.0015, 0.0015 );
	ADrawing::setColor( 1, 0, 1 );				// Draw prosoma center.
	ADrawing::drawSphere();
	glPopMatrix();
	
	glDisable( GL_LIGHTING );
	glBegin( GL_LINES );
	glColor3f( 1, 1, 0.0 );						// Draw a line.
	glVertex3d( Pw[X], Pw[Y], Pw[Z] );
	glVertex3d( to[X], to[Y], to[Z] );
	glEnd();
	glEnable( GL_LIGHTING );
	

}


/**
 * @brief Encodes a collision code so that the nearcallback function in ODE can identify which 
 * geometry in the spider's body is colliding.
 * @param group one of TRUNK or LEG.
 * @param member one of PROSOMA, FEMUR, TIBIA, etc.
 * @return one byte code 0xGM, where G is the group, and M is the member.
 */
unsigned char ASpiderSystem::encodeCollisionCode( int group, int member )
{
    unsigned char code = 0x0F;
    code &= group;              //The most significant 4 bits correspond to the group.
    code <<= 4;
    code |= (0x0F & member );   //The least significan 4 bits correspond to the member.
    return code;
}


/**
 * @brief Decodes a collision code so that the nearcallback function in ODE can tell which geometry
 * in the spider's body is in collision.
 * @remarks This function has to be used together with encodeCollisionCode.
 * @param code 0xGM (8 bits).
 * @param group outputs the 4 most significant bits of collision code.
 * @param member outputs the 4 least significant bits of collision code.
 */
void ASpiderSystem::decodeCollisionCode( unsigned char code, int* group, int* member )
{
    *group = code >> 4;
    *member = 0x0F & code;
}


/**
 * @brief Gets world coordinates of an endpoint in the given leg.
 * @param leg leg index from L_LEG1 to R_LEG4.
 * @param link which link along the leg the endpoint will be computed on.
 * @param ep one of the EndPoint enumerate constants: EP_TIP or EP_BASE.
 * @return endpoint in world coordinates.
 */
AVector3D ASpiderSystem::getWEndPointOf( int leg, int link, EndPoint ep )
{
	double length = -1;
    switch( link )                                      // Compute total length of link.
    {
        case FEMUR:
			length = femurLengths[leg] + 2.0*legLinkRadii[FEMUR] - joints[leg][link].radius - joints[leg][link+1].radius;
			break;
        case TIBIA:
			length = tibiaLengths[leg] + 2.0*legLinkRadii[TIBIA] - joints[leg][link].radius - joints[leg][link+1].radius;
			break;
        case METATARSUS:
			length = metatarsusLengths[leg] + 2.0*legLinkRadii[METATARSUS] - joints[leg][link].radius - joints[leg][link+1].radius;
			break;
        case TARSUS:
			length = tarsusLengths[leg] + 2.0*legLinkRadii[TARSUS] - joints[leg][link].radius;
    }
    
    dVector3 p;
	
	if( ep == EP_TIP )									// Tip of the link?
		dBodyGetRelPointPos( objects[leg][link].body, 0, 0, length/2.0, p );	// To world coordinates.
	else
		dBodyGetRelPointPos( objects[leg][link].body, 0, 0, -length/2.0, p );	// To world coordinates.
	
	return AVector3D( p );
}


/**
 * @brief Gets world coordinates of a point given in prosoma coordinates.
 * @param P point in prosoma coordinates.
 * @return point in world coordinates.
 */
AVector3D ASpiderSystem::getWorldFromProsomaCoordinates( const AVector3D& P )
{
    dVector3 w;
    dBodyGetRelPointPos( objects[TRUNK][PROSOMA].body, P[X], P[Y], P[Z], w );
    return AVector3D( w );
}

/**
 * @brief Main simulation spider system function.
 * @remarks This function is called by the ODE simulation step.
 */
void ASpiderSystem::simulate()
{
    ////////////////////////////////// Excute regulation modules ///////////////////////////////////

	regulationModule();
	heightRegulationModule();
	orientationModule();
	 
    //////////////////////////////////// Execute gait generator ////////////////////////////////////
    
    for( int I = L_LEG1; I <= LEG_COUNT; I++ )
    {
        switch( ikBodyParts[I].state )						// Determine locomotion behavior depending on regulation module.
        {
			//-------------------------------- Non-error states ----------------------------------//
				
			case S_INIT:									// Starting up system -- reaching desired angles.
				if( legIsGrounded( I ) )					// Verify if leg got supported.
				{
					ikBodyParts[I].state = S_STANCE;		// Change.
					cout << "Leg " << I << " is now on STANCE!" << endl;
				}
				else
				{
					beganFalling = true;						// Indicate we can check for velocities zero.
					for( int J = COXA; J <= ANKLE; J++ )		// Continue bending joints as the leg has not stopped.
					{
						double angle1 = fabs( dJointGetUniversalAngle1( joints[I][J].joint ) );
						if( angle1 < fabs( initDesiredLegAngles[J] ) )
							joints[I][J].desiredAngle[AXIS_1] += initDesiredLegAngles[J]/50.0;  // Reach desired angles in 50 steps.
					}
				}
				break;
				
			case S_STEPPING:										// Leg is in swing.
				if( legIsGrounded( I ) )							// Leg got support.
				{
					ikBodyParts[I].state = S_STANCE;				// Change state.
					ikBodyParts[I].minimumDistanceToStop = 0.0;     // Change value for minimum distance to stop.
					ikBodyParts[I].traveledDistance = 0.0;			// Reset traveled distance along a parametric curve.
					ikBodyParts[I].hermiteCurve.resetAndDisable();	// Erase previous curve.
					cout << "Leg " << I << " is now on STANCE!" << endl;
					nLegsStepping--;								// One less leg stepping.
				}
				break;
				
			case S_READY:
			case S_STANCE:
				break;
				
			//------------------------------------ Error states ----------------------------------//
				
            case S_LIMBO:
            {
				double length;
                ikBodyParts[I].state = S_STEPPING;					// Change leg state to stepping.
				ikBodyParts[I].traveledDistance = 0.0;				// Reset traveled distance.

				AVector3D endEffector = getWEndPointOf( I, TARSUS, EP_TIP );     // Tarsus tip in world coordinates.
                
                CurveData curveData = ikBodyParts[I].hermiteCurve.getLastCurveData();
				
				switch ( curveData.curveType )
                {
                    case CURVE_NONE:								// This happens when initializing the spider.
                    {
                        // Target node in world coordinates.
                        dVector3 profileArray;
                        dBodyGetRelPointPos( objects[TRUNK][PROSOMA].body, profile[I][X], profile[I][Y], profile[I][Z], profileArray );
                        AVector3D target( profileArray );
						AVector3D direction = target - endEffector;
						
						// Normal vector to ellipse.
						dVector3 upArray;
						dBodyGetRelPointPos( objects[TRUNK][PROSOMA].body, 0, 1, 0, upArray );
						AVector3D normal = AVector3D( upArray ).cross( direction );
						
						// Install ellipse.
						length = setEllipseOnLeg( I, direction, normal, THREE_EIGHTS * direction.norm(), SEARCH_STEP_SPEED );
                        break;
                    }
					case CURVE_ELLIPSE:								// Leg was stepping, but it stopped without being grounded.
					{
						// Install ellipse, but use previous direction slightly modified.
						length = setEllipseOnLeg( I );
						break;
					}
                    case CURVE_ARC:
                        break;
                }
				
				// Prepare time.
				ikBodyParts[I].ikTime = 0.0;						// Local time controller.
				
				// Minimum distance to allow stop recognition.
				ikBodyParts[I].minimumDistanceToStop = MIN_DISTANCE_STOP_PERCENT * length;
				
				cout << "Leg " << I << " is now STEPPING! Distance: " << length << endl;
				
				nLegsStepping++;									// One more leg stepping.
				
                break;
            }
        }
    }
	
	// Execute a walking sequence if flag is active.
	AVector3D dir;
	dir[X] = 3;
	dir[Y] = 2;
	walkTowards( dir );
	
	// Stepping legs (change desired angles by means of inverse kinematics).
	steppingModule();
	
	// Since we want all joint angles to remain in their desired angles, we have to make sure
	// to apply desired velocities.
	maintainDesiredAngles();
}

/**
 * @brief execute walking sequence in the given direction.
 * @remarks the norm of direction vector is taken as the width of the ellipse.
 * @param direction direction vector in world coordinates.
 */
void ASpiderSystem::walkTowards( const AVector3D& direction )
{
	if( walk > 0 )				// Activate sequence as long as the walk flag is active.
	{
		////////////// Verify that direction is not too far from heading or too long ///////////////
		
		double width = direction.norm();
		
		// Adjust direction vector.
		dVector3 directionLArray;
		dBodyVectorFromWorld( objects[TRUNK][PROSOMA].body, direction[X], direction[Y], direction[Z], directionLArray );
		double angle = atan2( -directionLArray[Z], directionLArray[X] );	// Project onto xz plane and find out the angle wrt heading.
		
		double maxAngle = walk * MAX_TURNING_ANGLE;				// The faster the waling, the wider the turning angle.
		angle = min( maxAngle, max( -maxAngle, angle ) );		// Clamp turning angle.
		
		dVector3 wDirectionArray;
		dBodyVectorToWorld( objects[TRUNK][PROSOMA].body, cos( angle ), 0, -sin( angle ), wDirectionArray );	// New direction.
		AVector3D wDirection( wDirectionArray );				// in world coordinates, and with the original length.
		wDirection = wDirection * width;
		
		////////////////// Move legs one by one in sequence if conditions are met //////////////////
		
		while( true )								// In case of delay = 0, activate opposite tetrapods altogether.
		{
			if( steppingSequence[0].second > 0 )	// If there was a delay, discount a time step from front leg in sequence.
				steppingSequence[0].second--;
			
			if( nLegsStepping < LEG_COUNT/4 )		// There is enough support to continue stepping?
			{
				if( steppingSequence[0].second <= 0 )							// Delay was met?
				{
					pair< int, int >front = steppingSequence[0];				// Next leg to step.
					int L = front.first;										// Leg index.
					int contraL = contralateralLeg( L );						// Contralateral leg must be on stance.
					
					if( ikBodyParts[L].state == S_STANCE && ikBodyParts[contraL].state == S_STANCE )	// This leg must be in stance.
					{
						steppingSequence.erase( steppingSequence.begin() );		// Remove it from front.
						steppingSequence.push_back( front );					// Push it back.
						steppingSequence.front().second = walkDelay;			// Add delay to new front.
						
						ikBodyParts[L].state = S_STEPPING;
						ikBodyParts[L].traveledDistance = 0.0;					// Prepare new stepping leg.
						
						dVector3 upArray;
						dBodyGetRelPointPos( objects[TRUNK][PROSOMA].body, 0, 1, 0, upArray );
						AVector3D normal = AVector3D( upArray ).cross( wDirection );		// Normal vector to ellipse.
						
						double ellipseSpeed = walk * IK_BASE_SPEED;				// Install ellipse.
						double length = setEllipseOnLeg( L, wDirection, normal, THREE_EIGHTS * width, ellipseSpeed );
						
						ikBodyParts[L].ikTime = 0.0;							// Local time controller.
						ikBodyParts[L].minimumDistanceToStop = MIN_DISTANCE_STOP_PERCENT * length;	// Min. dist. to allow stop checks.
						
						dJointDestroy( joints[L][CLAW].joint );					// Loose claw.
						joints[L][CLAW].joint = NULL;
						
						cout << "Leg " << L << " is now STEPPING! Distance: " << length << endl;
						
						nLegsStepping++;										// One more leg stepping.
					}
					else
						break;
				}
				else
					break;
			}
			else
				break;
		}
	}
}


/**
 * @brief Finds out the contralateral leg for a given input leg index.
 * @param L leg index from L_LEG1 to R_LEG4.
 */
int ASpiderSystem::contralateralLeg( int L )
{
	if( L > LEG_COUNT/2 )		// Right legs?
		return L - LEG_COUNT/2;
	
	return L + LEG_COUNT/2;		// Left legs.
}


/**
 * @brief Regulation module that determines if a leg enters an error state at each time step.
 */
void ASpiderSystem::regulationModule()
{
	for( int L = L_LEG1; L <= LEG_COUNT; L++ )
	{
		switch( ikBodyParts[L].state )		// We don't deal with error states here -- it is the Gait Generator who takes care of them.
		{
			case S_INIT:
				if( beganFalling && legHasStopped( L ) && !legIsGrounded( L ) )	// Leg came to stop and is not grounded.
				{
					ikBodyParts[L].state = S_LIMBO;								// Leg is in limbo.
					cout << "Leg " << L << " is in LIMBO! It was INIT previously!" << endl;
				}
				break;
			
			case S_STEPPING:
				if( ikBodyParts[L].traveledDistance > ikBodyParts[L].minimumDistanceToStop && legHasStopped( L ) && !legIsGrounded( L ) )
				{
					ikBodyParts[L].state = S_LIMBO;								// Leg came to stop after completing its path.
					cout << "Leg " << L << " is in LIMBO! It was STEPPING previously!" << endl;
				}
				break;
		
			case S_READY:
			case S_STANCE:
				if( !legIsGrounded( L ) )										// Leg lost support.
				{
					ikBodyParts[L].state = S_LIMBO;								// Leg is in limbo.
					cout << "Leg " << L << " is in LIMBO! It was STANCE before!" << endl;
				}
				else
				{
					if( getFootToProfileDistance( L ) > profileSphereRadius )	// Leg is far from profile center.
					{
						ikBodyParts[L].state = S_IMBALANCE;						// Leg is imbalanced.
						cout << "Leg " << L << " is IMBALANCED!" << endl;
					}
				}
				break;
		}
	}
}


/**
 * @brief Height regulation module to determine an external force that pushes the spider body 
 * towards a desired position above the average grounded-leg positions.
 */
void ASpiderSystem::heightRegulationModule()
{
	int anchoredLegsL = 0;
	int anchoredLegsR = 0;
	
	AVector3D sumAnchored, sumAll;
	
	for( int I = L_LEG1; I <= LEG_COUNT; I++ )
	{
		AVector3D foot = getWEndPointOf( I, TARSUS, EP_TIP );
		dVector3 footP;
		dBodyGetPosRelPoint( objects[TRUNK][PROSOMA].body, foot[X], foot[Y], foot[Z], footP );	// Foot in prosoma coordinates.
		
		if( ikBodyParts[I].state == S_STANCE )		// Anchored leg?
		{
			sumAnchored = sumAnchored + AVector3D( footP );
			if( I < R_LEG1 )
				anchoredLegsL++;					// Left leg.
			else
				anchoredLegsR++;					// Right leg.
		}
		
		sumAll = sumAll + AVector3D( footP );		// Acummulate all legs sum.
	}
	
	rHeightOld = rHeight;							// Preserve previous height distance.
	if( anchoredLegsL > 1 && anchoredLegsR > 1 )	// Regulate height only if there's at least two anchored legs on each side.
	{
		sumAnchored = sumAnchored / ( anchoredLegsL + anchoredLegsR );		// Average tarsus tips in prosoma coordinates.
		
		double C = sumAnchored[Y] + HEIGHT_REST_LENGTH;						// Desired prosoma COM with respect to avgPP.
		
		double fp = C * HEIGHT_KP;					// Spring force, trying to keep a zero-rest-length spring between C and COM.
		rHeight = -C;
		double fd = ((rHeight - rHeightOld)/timeStep) * HEIGHT_KD;
		double f = fp - fd;							// Spring minus damping force.
		dBodyAddRelForce( objects[TRUNK][PROSOMA].body, 0, f, 0 );								// In prosoma coordinates.
	}
	else
		rHeight = 0;								// Body might be freely falling.
	
	/////////////////////////// Drag body where all legs are leading to ////////////////////////////
	
	rFollowOld = rFollow;							// Preserve old follow distance.
	if( anchoredLegsL > 0 && anchoredLegsR > 0 )	// At least one leg per side is achored?
	{
		AVector3D Q = sumAll / LEG_COUNT;			// Centroid for all legs, in prosoma coordinates.
		Q[Y] = 0;									// Project onto xz plane.
		AVector3D prosomaCenter;
		prosomaCenter[X] = -0.000479276;			// Displace prosoma center a little bit.
		rFollow = Q - prosomaCenter;				// Distance from current prosoma center to expected centroid.
		
		AVector3D f = rFollow * FOLLOW_KP - (( rFollow - rFollowOld )/timeStep) * FOLLOW_KD;
		dBodyAddRelForce( objects[TRUNK][PROSOMA].body, f[X], 0, f[Z] );	// Force in prosoma coordinates.
	}
	else
		rFollow = zeroVector3D;						// Spider does not have any support, it is moving freely.
}


/**
 * @brief Orientation module that keeps the spider body stable with respect to right and left legs, 
 * and to front and back legs.
 */
void ASpiderSystem::orientationModule()
{
	vector< AVector3D >left, right, front, back;	// Store legs on the left and right of the prosoma.
	anchoredLegsPerSide( left, right, front, back );
	
	size_t anchoredLegsL = left.size();
	size_t anchoredLegsR = right.size();
	size_t anchoredLegsF = front.size();
	size_t anchoredLegsB = back.size();
	
	//////////////////////////////////// Around prosoma x-axis /////////////////////////////////////
	
	if( anchoredLegsL > 1 && anchoredLegsR > 1 )	// Enough support?
	{
		AVector3D R;								// Average right-positioned legs.
		for( int I = 0; I < anchoredLegsR; I++ )
			R = R + right[I];
		R = R / anchoredLegsR;
		R[X] = 0;									// Project onto zy plane.
		
		AVector3D L;								// Average left-positioned legs.
		for( int I = 0; I < anchoredLegsL; I++ )
			L = L + left[I];
		L = L / anchoredLegsL;
		L[X] = 0;									// Project onto zy plane.
		
		AVector3D v = R - L;						// Expected leaning of the prosoma.
		double theta = atan2( v[Y], v[Z] );			// Leaning angle.
		theta = min( MAX_TURNING_ANGLE, max( -MAX_TURNING_ANGLE, theta ) );		// Clamp angle to maximum turning angle.
		
		double torque = -theta * ORIENTATION_KP;
		dBodyAddRelTorque( objects[TRUNK][PROSOMA].body, torque, 0, 0 );
	}
	
	//////////////////////////////////// Around prosoma z-axis /////////////////////////////////////
	
	if( anchoredLegsF > 1 && anchoredLegsB > 1 )	// Enough support?
	{
		AVector3D F;								// Average front-positioned legs.
		for( int I = 0; I < anchoredLegsF; I++ )
			F = F + front[I];
		F = F / anchoredLegsF;
		F[Z] = 0;									// Project onto xy plane.
		
		AVector3D B;								// Average back-positioned legs.
		for( int I = 0; I < anchoredLegsB; I++ )
			B = B + back[I];
		B = B / anchoredLegsB;
		B[Z] = 0;									// Project onto xy plane.
		
		AVector3D v = F - B;						// Expected leaning of the prosoma.
		double phi = atan2( v[Y], v[X] );			// Leaning angle.
		phi = min( MAX_TURNING_ANGLE, max( -MAX_TURNING_ANGLE, phi ) );		// Clamp angle to maximum turning angle.
		
		double torque = -phi * ORIENTATION_KP;
		dBodyAddRelTorque( objects[TRUNK][PROSOMA].body, 0, 0, torque );
	}
}


/**
 * @brief Counts how many legs are anchored on the left and on the right side of the spider. It also
 * counts how many legs are in front and behind the prosoma. Stores the prosoma coordinates of each
 * foot in the corresponding vector.
 * @param l outputs anchored left legs.
 * @param r outputs anchored right legs.
 * @param f outputs anchored front legs.
 * @param b outputs anchored back legs.
 */
void ASpiderSystem::anchoredLegsPerSide( vector< AVector3D >&l, vector< AVector3D >&r, vector< AVector3D >&f, vector< AVector3D >&b )
{
	l.clear();
	r.clear();
	f.clear();
	b.clear();
	
	for( int L = L_LEG1; L <= LEG_COUNT; L++ )
	{
		if( ikBodyParts[L].state == S_STANCE )		// Anchored legs are on stance.
		{
			AVector3D foot = getWEndPointOf( L, TARSUS, EP_TIP );
			dVector3 footP;
			dBodyGetPosRelPoint( objects[TRUNK][PROSOMA].body, foot[X], foot[Y], foot[Z], footP );	// Foot in prosoma coordinates.
			
			if( footP[Z] < 0 )						// Left leg.
				l.push_back( AVector3D( footP ) );
			else
				r.push_back( AVector3D( footP ) );	// Right leg.
			
			if( footP[X] < 0 )						// Back leg.
				b.push_back( AVector3D( footP ) );
			else
				f.push_back( AVector3D( footP ) );	// Front leg.
		}
	}
}


/**
 * @brief Stepping module that moves legs by activating the inverse kinematics engine.
 */
void ASpiderSystem::steppingModule()
{
	// Step legs whose state is S_STEPPING.
	for( int L = L_LEG1; L <= LEG_COUNT; L++ )
	{
		if( ikBodyParts[L].state == S_STEPPING )
			ikBodyParts[L].traveledDistance = jacobianInverseKinematics( L );
	}
}


/**
 * @brief Performs the inverse kinematics engine by using the Inverse Jacobian Method.
 * @remarks Input leg must be stepping.
 * @param L leg index from L_LEG1 to R_LEG4.
 * @return cummulative traversed distance along the Hermitian path that describes the IK engine.
 */
double ASpiderSystem::jacobianInverseKinematics( int L )
{
    double s = ikBodyParts[L].hermiteCurve.ease( ikBodyParts[L].ikTime );   // Distance should have traveled so far.
    double u = ikBodyParts[L].hermiteCurve.U( s );                          // Parametric curve parameter for given s.
    AVector3D goal = ikBodyParts[L].hermiteCurve.P( u );                    // Point where end effector should be
                                                                            // in local coordinates.
	
	AVector3D endEffector;													// Tip of tarsus.
	vector< AVector3D > jointsLocal;
	
	dBodyID prosomaBody = prosomaBody = objects[TRUNK][PROSOMA].body;		// We keep a reference to prosoma body ptr.
	dVector3 jW, jP;														// Joint location in world and prosoma coordinates.
	for( int I = COXA; I <= ANKLE; I++ )
	{
		dJointGetUniversalAnchor( joints[L][I].joint, jW );					// Joint in world coordinates.
		dBodyGetPosRelPoint( prosomaBody, jW[X], jW[Y], jW[Z], jP );		// Joint in prosoma coordinates.
		jointsLocal.push_back( AVector3D( jP ) );
	}
	
	dVector3 eP;															// Coordinates of the end effector (tarsus).
	endEffector = getWEndPointOf( L, TARSUS, EP_TIP );						// Tarsus tip in world coordinates.
	dBodyGetPosRelPoint( prosomaBody, endEffector[X], endEffector[Y], endEffector[Z], eP );
	endEffector = AVector3D( eP );											// Tarsus tip in prosoma coordinates.
	
    ////////////////////////////////////// Defining the Jacobian ///////////////////////////////////
    
    // Collecting the axis-1 and axis-2 of the joints.
    const int TOTAL_ANGLES = 8;
    vector< AVector3D > axes;
	
	dVector3 axisW;
	AVector3D axisL;
	for( int I = 0; I < TOTAL_ANGLES; I++ )
	{
		int i = I % (TOTAL_ANGLES/2);									// Separate first axis from second axis.
		
		if( I < TOTAL_ANGLES/2 )
			dJointGetUniversalAxis1( joints[L][i].joint, axisW );
		else
			dJointGetUniversalAxis2( joints[L][i].joint, axisW );
		
		dVector3 axisLocal;
		dBodyVectorFromWorld( prosomaBody, axisW[X], axisW[Y], axisW[Z], axisLocal );   // Axis in prosoma coordinates.
		axisL = AVector3D( axisLocal );
		
		axes.push_back( axisL );										// Grow list of joints' axes.
	}
	
    // Filling in the Jacobian matrix.
    AMatrix< 3, 8 > J;                                                  // The Jacobian matrix.
    AVector3D column;
    for( int I = 0; I < TOTAL_ANGLES; I++ )
    {
        int i = I % (TOTAL_ANGLES/2);                                   // First 4 angles refer to rotations around axis 1.
        column = axes[I].cross( endEffector - jointsLocal[i] );         // Following 4 angles refer to rotations around axis 2.
        
        J.at( X, I ) = column[X];
        J.at( Y, I ) = column[Y];
        J.at( Z, I ) = column[Z];
    }
    
    //////////////////////////// Compute change in angles by using Jacobian ////////////////////////
    
    AVector3D v = goal - endEffector;
    AMatrix< 8, 3 > JTranspose = J.transpose();
    AMatrix< 3, 3 > Identity = AMatrix< 3, 3 >::identity();				// 3x3 entity matrix.
    const double lambda = 0.03;												// Damping factor for least squares solution.
    AMatrix< 8, 1 >angleDot;											// Change in angles.
    
    angleDot = JTranspose * ( J*JTranspose + lambda*lambda*Identity ).inverse() * v;
    
    ////////////////////////// Change angles by means of desired velocities ////////////////////////
	
	for( int J = COXA; J <= ANKLE; J++ )								// Go downwards.
	{
		changeDesiredJointAngle( L, J, AXIS_1, angleDot[J] );			// First axis.
		changeDesiredJointAngle( L, J, AXIS_2, angleDot[J+4] );			// Second axis.
	}
	
	/////////////////////////////////// Advance IK simulation time /////////////////////////////////
    
    ikBodyParts[L].ikTime += timeStep;    // Move in local time for this articulated body.
	
	return  s;
}


/**
 * @brief Computes desired velocities for each joint in order to reach the expected, respective
 * angles
 */
void ASpiderSystem::maintainDesiredAngles()
{
	for( int L = L_LEG1; L <= LEG_COUNT; L++ )
	{
		for( int J = COXA; J <= ANKLE; J++ )
		{
			double forceAxis1 = ikBodyParts[L].fMax[AXIS_1];
			double forceAxis2 = ikBodyParts[L].fMax[AXIS_2];
			
			double theta = dJointGetUniversalAngle1( joints[L][J].joint );      // Current angles of Jth joint on Lth body part.
			double phi = dJointGetUniversalAngle2( joints[L][J].joint );
			
			// Recalculate angle changes.
			double deltaTheta = joints[L][J].desiredAngle[AXIS_1] - theta;
			double deltaPhi = joints[L][J].desiredAngle[AXIS_2] - phi;
			
			if( ikBodyParts[L].state != S_STANCE )								// Increase joint force?
			{
				forceAxis1 *= JOINT_FORCE_STRONG_FACTOR;
				forceAxis2 *= JOINT_FORCE_STRONG_FACTOR;
			}
			else
			{
				if( J == COXA )		// Loose coxa joints in stance legs.
				{
					forceAxis1 /= 2.0;
					forceAxis2 /= 2.0;
				}
			}
			
			// Set a maximal force to achieve desired velocity.
			dJointSetUniversalParam( joints[L][J].joint, dParamFMax1, forceAxis1 );
			dJointSetUniversalParam( joints[L][J].joint, dParamFMax2, forceAxis2 );
			
			// Set a desired velocity so that we can reach the desired position.
			double velTheta = max( min( deltaTheta / timeStep, MAX_JOINT_VEL ), -MAX_JOINT_VEL );	// Clamp desired velocities.
			double velPhi = max( min( deltaPhi / timeStep, MAX_JOINT_VEL ), -MAX_JOINT_VEL );
			
			dJointSetUniversalParam( joints[L][J].joint, dParamVel1, velTheta );
			dJointSetUniversalParam( joints[L][J].joint, dParamVel2, velPhi );
		}
	}
}


/**
 * @brief Modifies the expected, desired angle in a joint.
 * @remarks This function does not apply any force on the joint. Use maintainDesiredAngles for that.
 * @param L leg index from L_LEG1 to R_LEG4.
 * @param joint joint index along the leg, from COXA to ANKLE.
 * @param axis one of the axis constants: AXIS_1 or AXIS2.
 * @param deltaAngle expected change in current angle of the joint in leg L.
 */
void ASpiderSystem::changeDesiredJointAngle( int L, int joint, int axis, double deltaAngle )
{
    double angle = 0.0;
	
	if( axis == AXIS_1 )
		angle = dJointGetUniversalAngle1( joints[L][joint].joint );
	else
		angle = dJointGetUniversalAngle2( joints[L][joint].joint );
    
    joints[L][joint].desiredAngle[axis] = max( min( legJointLimits[joint][HI_STOP][axis], angle + deltaAngle ),
												 legJointLimits[joint][LO_STOP][axis] );
}


/**
 * @brief Handles collision detection and creation of claw joints when a tarsus is grounded.
 * @remarks This function must be invoked in the nearcallback method in the ODE class.
 * @param part any spider body part that is in collision.
 * @param link any of the links along the body part that is in collision.
 * @param targetGeom ODE geometry pointer which the spider link is colliding with.
 * @param contactInfo vector with information from all contacts.
 * @return true if a claw joint has been created or one exists and will stay there; false otherwise.
 */
bool ASpiderSystem::inCollision( int part, int link, dGeomID targetGeom, const vector< dContactGeom >& contactInfo )
{
    bool jointExists = false;
    
    if( part >= L_LEG1 && part <= R_LEG4 )								// Filter out body parts that are not legs.
    {
        if( link == TARSUS )											// Filter out leg links that are not the tarsus.
        {
            if( joints[part][CLAW].joint == NULL && ( ikBodyParts[part].state != S_STEPPING ||	// Verify there is no joint created yet.
				(ikBodyParts[part].state==S_STEPPING && ikBodyParts[part].traveledDistance>ikBodyParts[part].minimumDistanceToStop) ) )
            {
                // Find the closest contact point to the tip of the leg.
                double closestDistance = 1.0;
                double depth = 1.0;
				AVector3D normal;
                AVector3D closestContact;
                AVector3D tarsusTip = getWEndPointOf( part, link, EP_TIP );		// End effector position.
                for( int I = 0; I < contactInfo.size(); I ++ )
                {
                    AVector3D contactPosition( contactInfo[I].pos );    // Contact position.
                    AVector3D diff = tarsusTip - contactPosition;
                    double distance = diff.dot( diff );
                    if( distance < closestDistance * closestDistance )
                    {
                        closestDistance = distance;                     // Search for the minimum distance.
                        closestContact = contactPosition;               // We might use this contact to create the joint.
						normal = AVector3D( contactInfo[I].normal );	// Normal vector to collision parallel plane.
                    }
                    
                    if( contactInfo[I].depth < depth )                  // Check minimum interpenetration depth.
                        depth = contactInfo[I].depth;
                }
                
                // Verify the closest contact is below a threshold (near the "claw" of the "part" leg).
                if( closestDistance <= MIN_DISTANCE_CLAW_CONTACT && depth <= MAX_DEPTH_CLAW_CONTACT )
                {
					// Check whether the velocity reduced below a threshold.
					double velDiff = ikBodyParts[part].newCumulativeVelocity - ikBodyParts[part].oldCumulativeVelocity;
					double relVel = velDiff/ikBodyParts[part].oldCumulativeVelocity;
					
					if( legHasStopped( part ) || relVel < DECELERATION_THRESHOLD )
					{
						// Install a universal joint.
						joints[part][CLAW].joint = dJointCreateUniversal( world, 0 );						// Create joint (no group).
						dJointAttach( joints[part][CLAW].joint, objects[part][link].body, dGeomGetBody( targetGeom ) );  // Attach joint.
						
						// Define axes for joint.
						dVector3 headingArray;
						dBodyVectorToWorld( objects[TRUNK][PROSOMA].body, 1.0, 0.0, 0.0, headingArray );
						AVector3D heading( headingArray );				// Spider heading vector in world coordinates.
						normal = normal/normal.norm();					// Normalize normal vector.
						AVector3D headingProjOntoNormal = normal * ( heading.dot( normal ) );
						AVector3D projOntoCollisionPlane = heading - headingProjOntoNormal;
						AVector3D anotherAxis = normal.cross( projOntoCollisionPlane/projOntoCollisionPlane.norm() );
						anotherAxis = anotherAxis/anotherAxis.norm();	// Normalize the second axis.
						
						dJointSetUniversalAnchor( joints[part][CLAW].joint, closestContact[X], closestContact[Y], closestContact[Z] );
						dJointSetUniversalAxis1( joints[part][CLAW].joint, normal[X], normal[Y], normal[Z] );
						dJointSetUniversalAxis2( joints[part][CLAW].joint, anotherAxis[X], anotherAxis[Y], anotherAxis[Z] );
						
						// Other IK properties.
						joints[part][CLAW].desiredAngle = vector< double >{ 0.0, 0.0 };		// Initial desired angle for both axes.
						
						// Set feedback.
						dJointSetFeedback( joints[part][CLAW].joint, &joints[part][CLAW].feedBack );
						
						jointExists = true;							// There's now a joint attaching this claw to a geom.
					}
                }
            }
            else
            {
				if( joints[part][CLAW].joint != NULL )				// Is there a claw installed?
				{
					// Getting feedback on claw that is attached on environment.
					dJointFeedback* feedBack = dJointGetFeedback( joints[part][CLAW].joint );
					AVector3D clawForce = AVector3D( feedBack->f1 );
					
					// Get a vector that represents the tarsus unit direction.
					dVector3 baseArray;
					dJointGetUniversalAnchor( joints[part][ANKLE].joint, baseArray );
					AVector3D base( baseArray );
					AVector3D tip = getWEndPointOf( part, TARSUS, EP_TIP );
					AVector3D tarsusVector = base - tip;				// Vector goes from tip to base.
					tarsusVector = tarsusVector / tarsusVector.norm();	// Normalize tarsus vector.
					
					// Project claw force onto unit tarsus vector. If positive, claw is pushing. If negative, claw is pulling leg.
					double projection = clawForce.dot( tarsusVector );
					
					if( projection < -MAX_CLAW_FORCE )					// Detach claw?
					{
						dJointDestroy( joints[part][CLAW].joint );
						joints[part][CLAW].joint = NULL;
						jointExists = false;
						cout << "Max projection for leg " << part << " is " << projection << endl;
					}
					else
						jointExists = true;								// Joint can support the maximum force being applied to it.
				}
            }
        }
    }
	
    return jointExists;     // True if joint was created, false otherwise.
}


/**
 * @brief Gets the prosoma location in world coordinates.
 */
AVector3D ASpiderSystem::getPosition()
{
    return prosomaLocation;
}


/**
 * @brief Checks if a leg has come to stop.
 * @param L leg index from L_LEG1 to R_LEG4.
 * @return true if leg is stable, false otherwise.
 */
bool ASpiderSystem::legHasStopped( int L )
{
	return ikBodyParts[L].newCumulativeVelocity < EPS;			// Leg has stabilized?
}


/**
 * @brief Detects whether a leg is grounded. That is, if the leg found support to continue stance.
 * @param L leg index from L_LEG1 to R_LEG4.
 * @return true if leg has a claw, false otherwise.
 */
bool ASpiderSystem::legIsGrounded( int L )
{
	return joints[L][CLAW].joint != NULL;
}


/**
 * @brief Obtains the distance from tarsus tip to its leg's profile sphere origin.
 * @param L leg index from L_LEG1 to R_LEG4.
 * @return distance from tarsus tip to profile sphere origin.
 */
double ASpiderSystem::getFootToProfileDistance( int L )
{
	dVector3 pW;						// Profile center in world coordinates.
	dBodyGetRelPointPos( objects[TRUNK][PROSOMA].body, profile[L][X], profile[L][Y], profile[L][Z], pW );
	
	AVector3D p( pW );
	AVector3D d = getWEndPointOf( L, TARSUS, EP_TIP ) - p;		// Difference vector.
	return d.norm();					// Return the scalar distance.
}


/**
 * @brief Updates the record of cummulative velocity of end effectors in the spider.
 */
void ASpiderSystem::updateVelocities()
{
	for( int I = L_LEG1; I < objects.size(); I++ )			// Iterate over all legs.
	{
		ikBodyParts[I].oldCumulativeVelocity = ikBodyParts[I].newCumulativeVelocity;	// Back up previous velocity.
		double cumulativeVelocity = 0.0;

		// Add linear velocities.
		const double* v1 = dBodyGetLinearVel( objects[I][TARSUS].body );
		cumulativeVelocity += AVector3D( v1 ).dot( AVector3D( v1 ) );
			
		// Add angular velocities.
		const double* v2 = dBodyGetAngularVel( objects[I][TARSUS].body );
		cumulativeVelocity += AVector3D( v2 ).dot( AVector3D( v2 ) );
		
		ikBodyParts[I].newCumulativeVelocity = cumulativeVelocity;	// Update new velocity.
	}
}


/**
 * @brief Outputs all angles along a leg.
 * @param leg leg index from L_LEG1 to R_LEG4.
 * @return a string version of the angles for each joint and each axis on it.
 */
string ASpiderSystem::dumpAngles( int leg )
{
	ostringstream osL;								// Accumulate everything in an output string stream.
	
	if( leg >= L_LEG1 && leg <= LEG_COUNT )
	{
		osL << "Leg " << leg << ":\n";
		
		for( int J = COXA; J <= CLAW; J++ )
		{
			osL << "* [ " << J << " ]:  ";
			
			if( joints[leg][J].joint == NULL )		// Null joints don't have angles (like claws that are unattached).
				osL << "Empty\n";
			else
			{
				double angle1 = dJointGetUniversalAngle1( joints[leg][J].joint );		// Get both angles of universal joint.
				double angle2 = dJointGetUniversalAngle2( joints[leg][J].joint );
				
				osL << "Axis 1: " << angle1 << "   Axis 2: " << angle2 << "\n";
			}
		}
	}
	else
		osL << "Parameter " << leg << " is not a valid index!" << "\n";
	return osL.str();								// Return a sring equivalent to contents in output string stream.
}

/**
 * @brief Installs an ellipse at the tip of a leg.
 * @remarks Input vectors are given in world coordinates.
 * @param L is the leg index.
 * @param direction is the direction vector in world coordinates; its length is used to compute the large ellipse radius.
 * @param normal is the normal vector to the ellipse plane.
 * @param height is the smaller (i.e. y-axis) radius of the ellipse.
 * @param speed is the speed to traverse the ellipse while the IK engine is working.
 * @return the length of the ellipse.
 */
double ASpiderSystem::setEllipseOnLeg( int L, const AVector3D direction, const AVector3D normal, double height, double speed )
{
	assert( L >= L_LEG1 && L <= LEG_COUNT );
	
	AVector3D directionL;						// Direction in local coordinates.
	AVector3D normalL;							// Normal vector in local coordinates.
	AVector3D endEffectorL;						// Tip of tarsus or base of femur depending on direction, in local coordinates.
	double directionLength = direction.norm();
	
	if( directionLength < EPS )					// Use a random new direction from curve data.
	{
		directionL = ikBodyParts[L].hermiteCurve.getLastCurveData().direction;
		normalL = ikBodyParts[L].hermiteCurve.getLastCurveData().normal;
		directionL = directionL * random( 1.1, 1.5 );				// Increase direction length.
		
		const double tenDegrees = PI/18.0;
		double angleX = random( -tenDegrees, tenDegrees );
		double angleY = random( -tenDegrees, tenDegrees );
		double angleZ = random( -tenDegrees, tenDegrees );
		
		AMatrix< 3, 3 >Rx = get3DRotation( angleX, X );				// Rotation around prosoma x axis.
		AMatrix< 3, 3 >Ry = get3DRotation( angleY, Y );				// Rotation around prosoma y axis.
		AMatrix< 3, 3 >Rz = get3DRotation( angleZ, Z );				// Rotation around proxoma z axis.
		
		normalL = Rz * Ry * Rz * normalL;							// Rotate local normal vector.
		height = directionL.norm() * THREE_EIGHTS;					// New height.
		speed = ikBodyParts[L].hermiteCurve.getLastCurveData().v0;	// Speed.
		speed *= random( 1.05, 1.15 );
	}
	else
	{
		dVector3 dArray;
		dBodyVectorFromWorld( objects[TRUNK][PROSOMA].body, direction[X], direction[Y], direction[Z], dArray );
		directionL = AVector3D( dArray );							// Direction in prosoma coordinates.
		
		dVector3 nArray;
		dBodyVectorFromWorld( objects[TRUNK][PROSOMA].body, normal[X], normal[Y], normal[Z], nArray );
		normalL = AVector3D( nArray );								// Normal in prosoma coordinates.
	}
	
	// Foot location.
	AVector3D foot = getWEndPointOf( L, TARSUS, EP_TIP );
	dVector3 fArray;
	dBodyGetPosRelPoint( objects[TRUNK][PROSOMA].body, foot[X], foot[Y], foot[Z], fArray );
	endEffectorL = AVector3D( fArray );
	
	// Clamp parameters to avoid overstretching and very fast steps.
	speed = min( MAX_STEP_SPEED, speed );							// Clamp speed.
	double legLength = femurLengths[L] + tibiaLengths[L] + metatarsusLengths[L] + tarsusLengths[L];
	if( directionLength > legLength/4.0 )							// Clamp direction.
	{
		directionL = (legLength/4.0) * (directionL/directionLength);
		height = (legLength/4.0) * THREE_EIGHTS;
	}
	
	// Install ellipse.
	return ikBodyParts[L].hermiteCurve.setEllipse( endEffectorL, directionL, height, normalL, speed,
												  EASING_IN_PERCENT, EASING_OUT_PERCENT );
}


/**
 * @brief Gets the transformation matrices that align the world frame with the claw joint coordinate
 * frame.
 * @param claw ODE joint pointer to a claw joint.
 * @param T outputs the 3x1 translation vector.
 * @param R outputs the 3x3 rotation matrix
 */
void ASpiderSystem::getClawMatrices( const dJointID claw, AVector3D& T, AMatrix< 3, 3 >& R )
{
	assert( claw );								// Verify claw joint is not NULL.
	
	dVector3 anchor;
	dJointGetUniversalAnchor( claw, anchor );	// World coordinates of the joint anchor.
	T = AVector3D( anchor );					// Translation vector.
	
	dVector3 a1, a2;
	dJointGetUniversalAxis1( claw, a1 );		// World coordinates of both claw axes.
	dJointGetUniversalAxis2( claw, a2 );
	AVector3D axis1( a1 );						// Normalize axes.
	AVector3D axis2( a2 );
	axis1 = axis1 / axis1.norm();
	axis2 = axis2 / axis1.norm();
	AVector3D axis3 = axis1.cross( axis2 );		// Generate a third axis, perpendicular to previous two.
	axis3 = axis3 / axis3.norm();
	
	double rot[] = { axis1[X], axis2[X], axis3[X],
					 axis1[Y], axis2[Y], axis3[Y],
					 axis1[Z], axis2[Z], axis3[Z] };
	R = AMatrix< 3, 3 >( rot );					// Rotation matrix.
}


/**
 * @brief Gets world coordinates of a point expressed in local coordinates of a claw.
 * @param claw ODE joint pointer.
 * @param pl point in local coordinates.
 * @return point in world coordinates.
 */
AVector3D ASpiderSystem::getWorldPointFromLocalClaw( const dJointID claw, const AVector3D pl )
{
	AVector3D T;								// Translation vector that aligns world to claw.
	AMatrix< 3, 3 > R;							// Rotation matrix that aligns world to claw.
	getClawMatrices( claw, T, R );
	
	AVector3D pw = R * pl;
	pw = T + pw;
	
	return pw;
}


/**
 * @brief Gets the claw local coordinates of a point expressed in world coordinates.
 * @param claw ODE joint pointer.
 * @param pw point in world coordinates.
 * @return point in local coordinates.
 */
AVector3D ASpiderSystem::getLocalPointFromWorldClaw( const dJointID claw, const AVector3D pw )
{
	AVector3D T;								// Translation vector that aligns world to claw.
	AMatrix< 3, 3 > R;							// Rotation matrix that aligns world to claw.
	getClawMatrices( claw, T, R );
	
	AVector3D pl = -T + pw;
	pl = R.transpose() * pl;
	
	return pl;
}


/**
 * @brief Gets world coordinates of a vector expressed in local claw coordinates.
 * @param claw ODE joint pointer.
 * @param vl vector in local coordinates.
 * @return vector in world coordinates.
 */
AVector3D ASpiderSystem::getWorldVectorFromLocalClaw( const dJointID claw, const AVector3D vl )
{
	AVector3D T;								// Translatation vector that aligns world to claw.
	AMatrix< 3, 3 > R;							// Rotation matrix that aligns world to claw.
	getClawMatrices( claw, T, R );
	
	AVector3D vw = R * vl;
	return vw;
}


/**
 * @brief Gets claw local coordinates of a vector expressed in world coordinates.
 * @param claw ODE joint pointer.
 * @param vw vector in world coordinates.
 * @return vector in local coordinates.
 */
AVector3D ASpiderSystem::getLocalVectorFromWorldClaw( const dJointID claw, const AVector3D vw )
{
	AVector3D T;								// Translation vector that aligns world to claw.
	AMatrix< 3, 3 > R;							// Rotation matrix that aligns world to claw.
	getClawMatrices( claw, T, R );
	
	AVector3D vl = R.transpose() * vw;
	return vl;
}









