//
//  main.cpp
//  Arthropoda
//
//  Created by Luis Angel on 9/16/14.
//  Copyright (c) 2014 Luis Angel. All rights reserved.
//

#include "Arthropoda.h"

int main(int argc, char* argv[])
{
    Arthropoda::launchApplication( argc, argv );
    return 0;
}

