//
//  ACurve.cpp
//  Arthropoda
//
//  Created by Luis Angel on 10/14/14.
//  Copyright (c) 2014 Luis Angel. All rights reserved.
//

#include "ACurve.h"
using namespace ACurveNS;

/***************************************************************************************************
 Constructor.
***************************************************************************************************/
ACurve::ACurve()
{
    resetAndDisable();
}

/***************************************************************************************************
 Function to initialize the parametric curve.
 Input:
    v0 - the constant velocity.
    a - the percentage of the total arc length that must be used for easing-in.
    b - the percentage of the total arc length that must be used for easing-out.
 Output:
    totalDistance to travel by this curve.
***************************************************************************************************/
double ACurve::initCurve( double v0, double a, double b )
{
    assert( v0 > 0.0 );             // Constant velocity cannot be zero.
    assert( a > 0  && b > 0 );      // Easing in/out should always be defined.
    assert( a + b < 1.0 );          // Easing in/out cannot use up all time available for traversing the curve.
    
    constSpeed = v0;
    
    // Fill in the arc length table.
    totalDistance = arcLength.fillOut( (ACurve*) this, 20, numberOfSegments, &(this->Pstatic) );
    
    // Distances for easing-in/out.
    da = a * totalDistance;         // Distance to travel while easing-in.
    db = b * totalDistance;         // Distance to travel while easing-out.
    
    // Total time necessary to traverse the curve.
    totalTime = ( totalDistance + da + db ) / constSpeed;
    
    // Time marks for stopping/starting easing-in/out.
    ta = 2 * da / constSpeed;
    tb = ( totalDistance + da - db ) / constSpeed;
    
    // Return total distance to travel.
    return totalDistance;
}

/***************************************************************************************************
 Function to reset curve parameters and disable it.
***************************************************************************************************/
void ACurve::resetAndDisable()
{
    da = db = 0;                // Initialize easing in/out distances.
    ta = tb = 0;                // Initialize easing in/out time marks.
    totalDistance = 0;          // No total distance.
    totalTime = 0;              // No total time.
    numberOfSegments = 0;       // No segments assigned to curve.
    hermiteSegments.clear();    // Clean list of Hermite segments.
	
	// Reset curve data.
	curveData.curveType = CURVE_NONE;		// No curve assigned.
	curveData.easeIn = 0.0;					// Common data for all types of curves.
	curveData.easeOut = 0.0;
	curveData.v0 = 0.0;
	curveData.p0 = zeroVector3D;
	curveData.r0 = zeroVector3D;			// For arc/line.
	curveData.r1 = zeroVector3D;			// For arc/line.
	curveData.direction = zeroVector3D;
	curveData.normal = zeroVector3D;
}

/***************************************************************************************************
 Function to get a point on the curve, given parameter u.
***************************************************************************************************/
AVector3D ACurve::P( double u )
{
    assert( u >= 0 && u <= numberOfSegments );      // 0 <= u <= numberOfSegments.
    
    int segment = floor( u );                       // Which segment to query.
    if( segment == numberOfSegments )               // Let u within the last segment take the value of 1.
        segment--;
    
    double innerU = u - segment;                    // Subtract integer part and obtain 0 <= innerU <= 1.
    return hermiteSegments[ segment ].P( innerU );
}

/***************************************************************************************************
 Static wrapper-function used when filling out the arc length table.
***************************************************************************************************/
AVector3D ACurve::Pstatic( void* userData, double u )
{
    return static_cast< ACurve* >( userData )->P( u );
}

/***************************************************************************************************
 Function to implement easing in/out.
 Distance-time function for constant acceleration.
***************************************************************************************************/
double ACurve::ease( double t )
{
    assert( totalDistance > 0 );                    // Verify the arc length table is full.
    
    // Evaluate piecewise function.
    if( t <= 0.0 )
        return 0;                                   // No distance traveled yet.
    
    if( t > 0 && t < ta )                           // Easing-in.
        return constSpeed * t * t / (2.0 * ta);
    
    if( t >= ta && t <= tb )                        // Constant speed.
        return da + constSpeed * ( t - ta );
    
    if( t > tb && t < totalTime )                   // Easing out.
        return da + constSpeed * (tb - ta) + constSpeed * (t - tb) / (2.0 * (totalTime - tb)) * (2.0*totalTime - t - tb);
    
    return totalDistance;                           // The whole distance has been already traveled.
}

/***************************************************************************************************
 Function to return the u value of the curve, given a total distance traversed.
***************************************************************************************************/
double ACurve::U( double s )
{
    assert( totalDistance > 0 );                    // Verify the arch length table is full.
    assert( !( s < 0.0 || s > totalDistance ) );    // Check that s is in a valid range.
    
    return arcLength.U( s );
}

/***************************************************************************************************
 Function to draw the whole curve.
 Input:
    R - a pointer to a rotation matrix, which must have 12 elements (from ODE).
    T - a pointer to a translation vector (from ODE).
***************************************************************************************************/
void ACurve::drawCurve( const double* R, const double* T )
{
    if( totalDistance > 0 )                         // Draw it only if it has been initialized.
    {
        double u = 0.0;                             // Parameter.
        int nStepsPerSegment = 20;                  // How many evaluations per segment.
        double stepSize = 1.0 / ( nStepsPerSegment - 1.0 );
        
        glDisable( GL_LIGHTING );                   // No light to draw with raw colors.
        glColor3d( 0.7, 0.7, 0.7 );
        glBegin( GL_LINES );
        
        AVector3D P0, P1;
        
        P0 = P( u );                                // First evaluation.
        P0 = P0.transform( R, T );
        
        for( int segment = 0; segment < numberOfSegments; segment++ )
        {
            P1 = P( segment );                      // Initial point.
            P1 = P1.transform( R, T );              // Transform point.
            
            if( fabs( (P1 - P0).norm() ) > EPS )    // Draw a line only if there is a difference between points.
            {
                glVertex3d( P0[0], P0[1], P0[2] );  // A line will not draw when we first start evaluating the curve.
                glVertex3d( P1[0], P1[1], P1[2] );
            }
            
            P0 = P1;
            u = segment + stepSize;
            
            for( int I = 1; I < nStepsPerSegment - 1; I++ )
            {
                P1 = P( u );                        // Evaluate consecutive points.
                P1 = P1.transform( R, T );          // Transform point.
                
                glVertex3d( P0[0], P0[1], P0[2] );  // Draw in pairs.
                glVertex3d( P1[0], P1[1], P1[2] );
                
                P0 = P1;
                u += stepSize;
            }
        }
        
        P1 = P( numberOfSegments );                 // Interpolate last point.
        P1 = P1.transform( R, T );                  // Transform last point.
        
        glVertex3d( P0[0], P0[1], P0[2] );          // Draw in pairs.
        glVertex3d( P1[0], P1[1], P1[2] );
        
        glEnd();
        glEnable( GL_LIGHTING );                    // Go back to original lighting.
    }
}

/***************************************************************************************************
 Function to enable an ellipse. By default, ellipse is defined on the xy plane, and then is rotated
 and translated so that the z-axis matches the normal to the target plane.
 Input:
    p0 - the initial point in local coordinates.
	dir - direction vector. The length of dir is taken as the ellipse width.
    h - indicates ellipse height. It can't be 0. If positite, motion begins upwards. If negative, motion begins downwards.
    n - the normal to the plane on which the ellipse lies.
    v0 - the constant speed for traversing the ellipse.
    easeInD - the percentage of total distance to be used for easing in.
    easeOutD - the percentage of total distance to be used for easing out.
 Output:
    totalDistance to traver by this ellipse.
***************************************************************************************************/
double ACurve::setEllipse( AVector3D p0, AVector3D dir, double h, AVector3D n, double v0, double easeInD, double easeOutD )
{
	double w = dir.norm() / 2.0;
	
    assert( w != 0.0 && h != 0.0 );             // Width and height must be nonzero.
    assert( n.norm() > 0.0 );                   // Plane normal vector should not be zero.
    
    if( numberOfSegments > 0 )
        resetAndDisable();                      // Reset curve if number of segments is non zero.
    
    numberOfSegments = 4;                       // Ellipse is approximated with four Hermite segments.
    
    // Control points on the z-plane.
    AVector3D P0; P0[0] = w;
    AVector3D P1; P1[1] = h;
    AVector3D P2; P2[0] = -w;
    AVector3D P3; P3[1] = -h;
    
    // Tangent vectors on the z-plane.
    double scale = 1.63405;                     // To approximate curvature at each of the four control points.
    AVector3D R0; R0[1] = scale * h;
    AVector3D R1; R1[0] = -w * scale;
    AVector3D R2 = -R0;
    AVector3D R3 = -R1;
	
	// Create transformation matrix for points in ellipse.
	AVector3D dirHat = -dir / dir.norm();
	AVector3D nHat = n / n.norm();
	AVector3D y = nHat.cross( dirHat );
	double r[] = { dirHat[0], y[0], nHat[0],			// dir becomes x.
				   dirHat[1], y[1], nHat[1],			// normal becomes z.
				   dirHat[2], y[2], nHat[2] };			// y is the cross product z cross x.
	AMatrix< 3, 3 >RotZ( r );							// Transformation matrix.
	
	// Rotate and reflect control points and tangent vectors.
	P0 = RotZ * P0;
	P1 = RotZ * P1;
	P2 = RotZ * P2;
	P3 = RotZ * P3;
	
	R0 = RotZ * R0;
	R1 = RotZ * R1;
	R2 = -R0;
	R3 = -R1;
	
    // Compute distance between P0 and p0.
    AVector3D diff = p0 - P0;
    
    // Translate control points (tangents are vectors, and they're not affected by translation).
    P0 = P0 + diff;
    P1 = P1 + diff;
    P2 = P2 + diff;
    P3 = P3 + diff;
    
    // Create hermite segments.
    AHermiteSegment segment0( P0, P1, R0, R1 );
    AHermiteSegment segment1( P1, P2, R1, R2 );
    AHermiteSegment segment2( P2, P3, R2, R3 );
    AHermiteSegment segment3( P3, P0, R3, R0 );     // Last segment closes a loop.
    
    hermiteSegments.push_back( segment0 );
    hermiteSegments.push_back( segment1 );
    hermiteSegments.push_back( segment2 );
    hermiteSegments.push_back( segment3 );
    
    // Init arclentgh parameterization.
    double length = initCurve( v0, easeInD, easeOutD );      // Return total distance to travel in the ellipse.
    
    // Set new curve data.
    curveData.curveType = CURVE_ELLIPSE;
    curveData.p0 = p0;
    curveData.r0 = zeroVector3D;
    curveData.r1 = zeroVector3D;
    curveData.direction = dir;
    curveData.v0 = v0;
    curveData.easeIn = easeInD;
    curveData.easeOut = easeOutD;
	curveData.normal = n;
    
    return length;
}

/***************************************************************************************************
 Function to define a generic arc with a Hermite spline.
 Input:
    p0 - Initial point.
	dir - Direction vector.
    r0 - Initial tangent.
    r1 - Ending tangent.
    v0 - Constant velocity.
    easingInD - percentage of total distance to be used as easing in.
    easingOutD - percentage of total distance to be used as easing out.
 Output:
    totalDistance to travel by this arc.
***************************************************************************************************/
double ACurve::setArc( AVector3D p0, AVector3D dir, AVector3D r0, AVector3D r1, double v0, double easingInD, double easingOutD )
{
    assert( dir.norm() > 0.0 );					// Initial and ending points are, at least, different.
	
	AVector3D p1 = p0 + dir;					// Generate an ending point.
    
    if( numberOfSegments > 0 )
        resetAndDisable();                      // Reinitialize it if there are segments.
    
    numberOfSegments = 1;                       // An arc contains only one Hermite segment.
    
    AHermiteSegment segment( p0, p1, r0, r1 );
    hermiteSegments.push_back( segment );       // Attach unique segment to the list.
    
    double length = initCurve( v0, easingInD, easingInD );      // Intialize arc length parameterization.
    
    // Change curve data.
    curveData.curveType = CURVE_ARC;
    curveData.p0 = p0;
    curveData.r0 = r0;
    curveData.r1 = r1;
    curveData.direction = dir;
	curveData.normal = zeroVector3D;
    curveData.v0 = v0;
    curveData.easeIn = easingInD;
    curveData.easeOut = easingOutD;
    
    return length;
}

/***************************************************************************************************
 Function to return the last curve data.
***************************************************************************************************/
CurveData ACurve::getLastCurveData()
{
    return curveData;
}









