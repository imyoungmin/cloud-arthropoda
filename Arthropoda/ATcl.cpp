//
//  ATcl.cpp
//  Arthropoda
//
//  Created by Luis Angel on 9/16/14.
//  Copyright (c) 2014 Luis Angel. All rights reserved.
//

#include "ATcl.h"

SETVAR aScriptVariables[2];   //Defined in AOdeSystem class.
extern void openGLMainLoop();

/***************************************************************************************************
 Tcl/Tk global variables.
***************************************************************************************************/
Tcl_Interp* ATcl::aTclInterpreter = NULL;
int garbage = 10;
SETVAR ATcl::setlist[] = { "garbage", TCL_LINK_INT, (char *) &garbage, "", 0, (char *) NULL };

/***************************************************************************************************
 Function to initialize TCL and TK interpreters.
***************************************************************************************************/
int ATcl::tclTkInit( Tcl_Interp *interp )
{
    //Initialize the interpreter for TCL.
	if( Tcl_Init( interp ) == TCL_ERROR )
	{
		fprintf( stderr, "Tcl_Init error: %s\n", Tcl_GetStringResult( interp ) );
		exit( EXIT_FAILURE );
		return TCL_ERROR;
	}
    
    //Initialize the interpreter for TK.
    if( Tk_Init( interp ) == TCL_ERROR )
	{
		fprintf( stderr, "Tk_Init error: %s\n", Tcl_GetStringResult( interp ) );
		exit( EXIT_FAILURE );
		return TCL_ERROR;
	}
    
    //Creating the window for console and commands.
	char fileName[64];
	sprintf( fileName, "window.tcl" );
	if( Tcl_EvalFile( interp, fileName ) == TCL_ERROR )
    {
        fprintf( stderr, "Reading file error: %s\n", Tcl_GetStringResult( interp ) );
        exit( EXIT_FAILURE );
        return TCL_ERROR;
    }
    
    //Register user variables.
    linkUserVariables( interp );
    
    //Register internal/user commands.
    Tcl_CreateObjCommand( interp, "system", systemMethod, (ClientData) NULL, (Tcl_CmdDeleteProc *) NULL );
    Tcl_CreateObjCommand( interp, "evaluate", evaluateMethod, (ClientData) NULL, (Tcl_CmdDeleteProc *) NULL );
	Tcl_CreateObjCommand( interp, "quit", quitMethod, (ClientData) NULL, (Tcl_CmdDeleteProc *) NULL );
    
    aTclInterpreter = interp;
    
	outputMessage( "Arthropoda system launched" );
    
    openGLMainLoop();
	return TCL_OK;
}

/***************************************************************************************************
 Function to execute Tcl Tk idle event.
***************************************************************************************************/
void ATcl::tclTkEvents()
{
    while( Tk_DoOneEvent( TK_DONT_WAIT ) != 0 );
}

/***************************************************************************************************
 Function that links user variables.
***************************************************************************************************/
void ATcl::linkUserVariables( Tcl_Interp *interp )
{
	int	count =	0;
	for( int i = 0; setlist[i].ptr; i++ )
	{
		SETVAR *v = &setlist[i];
		if( Tcl_LinkVar( interp, v->name, v->ptr, v->type) == TCL_ERROR )
            outputMessage( "ERROR: Cannot link variables %s", v->name );
		else
			count++;
	}
	count = 0;
	for( int i = 0; setlist[i].ptr; i++ )
	{
		SETVAR *v = &aScriptVariables[i];
        
		if( Tcl_LinkVar( interp, v->name, v->ptr, v->type ) == TCL_ERROR )
			ATcl::outputMessage( "ERROR: Cannot link variables %s", v->name );
		else
			count++;
	}
    
	ATcl::outputMessage ( "Linked %d user Tcl variables", count );
}

/***************************************************************************************************
 Function to output a message on the window console.
***************************************************************************************************/
void ATcl::outputMessage( const char *format, ... )
{
    va_list vl;
    
	//format is the last argument specified; all others must be accessed using the variable-argument macros.
	va_start( vl, format );
	static char message[1024];
	static char message2[1024];
	vsprintf( message, format, vl );
	va_end( vl );
    
	int len = (int) strlen( message );
	if(len >= 1024)
	{
		strncpy( message2, message, 1022 );
		strcpy( message, message2 );
	}
    
	char command[1024];
    
	//Use lists etc to prevent the interpreter from substituting variables and interpreting special characters.
    //We must pass the actual command using { and } to avoid TCL substitution.
	sprintf( command, "lindex [list {%s}] 0", message );
    
	//Replace new lines with space.
	/*char *s = &command[0];
	for( ; *s != '\0'; s++ )
		if( *s == '\n' )
			*s = 32;
    */
    //Send command.
    executeCommand( command );
}

/***************************************************************************************************
 Function to execute a TCL command.
***************************************************************************************************/
void ATcl::executeCommand( const char *command )
{
    char newCommand[1024] = "";
    sprintf( newCommand, "evaluate {%s}", command );    //Attach to extended eval command.
	
    //Replace new lines with space.
	/*char *s = &newCommand[0];
	for( ; *s != '\0'; s++ )
		if( *s == '\n' )
			*s = 32;
    */
    //Send command.
    if ( ATcl::aTclInterpreter )
        Tcl_Eval( ATcl::aTclInterpreter, newCommand );
}

/***************************************************************************************************
 Function that implements the command 'evaluate' for TCL.
***************************************************************************************************/
int ATcl::evaluateMethod( ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[] )
{
    char command[1024] = "";
    char outputLineProc[1024] = "";
    
    /*The command name is objv[0] so 3 arguments are expected.*/
    if( objc != 2 )
        strcpy( command, "puts {Wrong number of parameters for evaluate command}" );
    else
    {
        int strLength;
        const char* str = Tcl_GetStringFromObj( objv[1], &strLength );
        strncpy( command, str, strLength );
    }
    
    Tcl_Eval( ATcl::aTclInterpreter, command );           //Evaluate the actual command that the user input to this function.
    
    strcpy( outputLineProc, "outputLine {" );       //Prepare a TCL instruction to output user command's result into console.
    char *s = &(ATcl::aTclInterpreter->result[0]);
    char *d = &(outputLineProc[ strlen( outputLineProc ) ]);
    for( ; *s != '\0'; s++ )                        //Copy result into the outputLineProc string.
    {
        *d = *s;
        d++;
    }
    *d = '\0';
    strcat( outputLineProc, "}" );
    Tcl_Eval( ATcl::aTclInterpreter, outputLineProc );    //Output user command's result to console text area in window.
    
    return TCL_OK;
}

/***************************************************************************************************
 Function that implements the command 'quit' for TCL.
***************************************************************************************************/
int ATcl::quitMethod( ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[] )
{
    exit( EXIT_SUCCESS );           //Exit application.
	return TCL_OK;
}

/***************************************************************************************************
 Function that implements the global 'system' command for TCL.
***************************************************************************************************/
int ATcl::systemMethod( ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[] )
{
    if( objc < 2 )
	{
		outputMessage( "Usage: system <name> ..." );
		return TCL_ERROR;
	}
	else
	{
        //Get system name.
        char* sysName = Tcl_GetString( objv[1] );
        ABaseSystem* sysPtr;
        
        //Search system in database.
        if( ( sysPtr = AManager::getSystem( sysName ) ) != NULL )
            return sysPtr->command( objc - 2, &objv[2] );
        else
            return TCL_ERROR;
	}
}












