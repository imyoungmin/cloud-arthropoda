//
//  Arthropoda.cpp
//  Arthropoda
//
//  Created by Luis Angel on 9/16/14.
//  Copyright (c) 2014 Luis Angel. All rights reserved.
//

#include "Arthropoda.h"

/***************************************************************************************************
 OpenGL main loop function to be used as an external function.
***************************************************************************************************/
void openGLMainLoop()
{
    glutMainLoop();
}

/***************************************************************************************************
 Global variables.
***************************************************************************************************/
//Initialize OpenGL global variables.
Timer Arthropoda::globalTimer = Timer();
FrameSaver* Arthropoda::frameSaver = new FrameSaver();
BallData* Arthropoda::arcBall = new BallData();
int Arthropoda::viewportWidth = 1420;
int Arthropoda::viewportHeight = 700;
int Arthropoda::mouseButton = -1;
float Arthropoda::zoom = 1;
int Arthropoda::previousY = 0;
bool Arthropoda::simulate = false;
bool Arthropoda::step = false;
bool Arthropoda::record = false;
double eyeArray[] = { 0.0, 0.01, 1.0 };
AVector3D Arthropoda::eye = AVector3D( eyeArray );
AVector3D Arthropoda::reference = zeroVector3D;
double Arthropoda::simulationTime = 0.0;
double Arthropoda::simulationStep = 0.002;
AOde* Arthropoda::ode = NULL;

/***************************************************************************************************
 Constructor.
 argc - number of arguments.
 argv - the actual arguments.
***************************************************************************************************/
int Arthropoda::launchApplication( int argc, char* argv[] )
{
    //Initialize OpenGL callback functions.
    glutInit( &argc, argv );
	glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH );
	glutInitWindowPosition( 10, 10 );
	glutInitWindowSize( viewportWidth, viewportHeight );
	glutCreateWindow( argv[0] );
    initOpenGl();
    resetTime();
	glutIdleFunc( idle );
	glutReshapeFunc( reshapeView );
	glutKeyboardFunc( keyPressed );
    glutSpecialFunc( specialKeyPressed );
	glutMouseFunc( mousePressed );
	glutMotionFunc( mouseMoved );
	glutDisplayFunc( render );
    
	//Init ODE.
    ode = new AOde( simulationStep );
    
    //Init Tcl/Tk.
    Tcl_Main( argc, argv, ATcl::tclTkInit );
	
    return 0;						//Never reached.
}

/***************************************************************************************************
 Function to set up material and lighting parameters in OpenGl
***************************************************************************************************/
void Arthropoda::initOpenGl()
{
	glShadeModel( GL_SMOOTH );								//Enable smooth shading.
	glClearColor( 0.2f, 0.2f, 0.2f, 1.0f );					//Black background.
	glClearDepth( 1.0f );									//Depth buffer setup.
	glHint( GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST );	//Really nice perspective calculations.
    
	GLfloat ambient[] = { 0.2, 0.2, 0.2, 1.0 };
	GLfloat diffuse[] = { 1.0, 1.0, 1.0, 1.0 };
	GLfloat specular[] = { 1.0, 1.0, 1.0, 1.0 };
	GLfloat position[] = { 0.0, 0.0, 1.0, 0.0 };
	GLfloat lmodel_ambient[] = { 0.2, 0.2, 0.2, 1.0 };
	GLfloat local_view[] = { 0.0 };
    
	//Set lighting parameters.
	glLightfv( GL_LIGHT0, GL_AMBIENT, ambient );
	glLightfv( GL_LIGHT0, GL_DIFFUSE, diffuse );
	glLightfv( GL_LIGHT0, GL_SPECULAR, specular );
	glLightf( GL_LIGHT0, GL_SHININESS, 100);
	glLightfv( GL_LIGHT0, GL_POSITION, position );
	
	glLightModelfv( GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
	glLightModelfv( GL_LIGHT_MODEL_LOCAL_VIEWER, local_view);
	glLightModeli( GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE );
    
	glEnable( GL_LIGHTING );
	glEnable( GL_LIGHT0 );
	glEnable( GL_AUTO_NORMAL );
	glEnable( GL_NORMALIZE );
	glEnable( GL_DEPTH_TEST );
	glDepthFunc( GL_LESS );
    
	glPixelStorei( GL_PACK_ALIGNMENT, 1 );
	glPixelStorei( GL_UNPACK_ALIGNMENT, 1 );
    
	Ball_Init( arcBall );
	Ball_Place( arcBall, qOne, 0.75 );
    
	glViewport( 0, 0, viewportWidth, viewportHeight );	//Init viewport.
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();
}

/***************************************************************************************************
 Function to render the OpenGL scene.
***************************************************************************************************/
void Arthropoda::render()
{
	double oldTime = globalTimer.GetElapsedTime();
    
	//Clear depth buffers.
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    
	//Load initial matrix transformation.
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
    
    //Locate the camera.
    gluLookAt( eye[0], eye[1], eye[2], reference[0], reference[1], reference[2], 0.0, 1.0, 0.0 );
    
	HMatrix arcballRot;
	Ball_Value( arcBall, arcballRot );
	glMultMatrixf( (float *)arcballRot );
    
	//Scale the scene in response to mouse commands.
	glScalef( zoom, zoom, zoom );
    
	/////////////////// Start drawing real scene //////////////////
    
	ode->drawODEScene();            //Draw ODE objects and creatures.
    
	//////////////////////// End of drawing ///////////////////////
    
	glMatrixMode( GL_PROJECTION );
	glPushMatrix();
	glLoadIdentity();
    
	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();
	glLoadIdentity();
    
	//Display frame rate, global time, and simulation time.
	glPushAttrib( GL_LIGHTING | GL_TEXTURE_2D );
	glDisable( GL_LIGHTING );
	glDisable( GL_TEXTURE_2D );
	glColor3f( 1.0, 1.0, 1.0 );
	//	fps  =  1/(TIME - oldTime);
	int fps  =  (int)( (float) 1.0 ) / ( globalTimer.GetElapsedTime() - oldTime );
	char s[80];
	sprintf( s, "FPS: %5d  TIME %6.3lf  SIMTIME %6.3lf%c", fps, globalTimer.GetElapsedTime(), simulationTime, '\0' );
	glRasterPos2f( -0.98, -0.98 );
	ADrawing::drawLabel( s, 12 );
	glPopAttrib();
    
	glMatrixMode( GL_PROJECTION );
	glPopMatrix();
	glMatrixMode( GL_MODELVIEW );
	glPopMatrix();
    
	glutSwapBuffers();
    
	if( record )							//Create movie if it is recording.
		frameSaver->DumpPPM( viewportWidth, viewportHeight );
}

/***************************************************************************************************
 Function to restore the view to the original orientation.
***************************************************************************************************/
void Arthropoda::restoreView()
{
	resetArcBall();			//Back to initial rotation.
	zoom = 1.0;				//Back to zoom 1.
	previousY = 0.0;
    reference = zeroVector3D;
    eye = AVector3D( eyeArray );
	glutPostRedisplay();
}

/***************************************************************************************************
 Function to reset timer.
***************************************************************************************************/
void Arthropoda::resetTime()
{
	globalTimer.GetElapsedTime();
	globalTimer.Reset();
}

/**********************************************************************************
 Function to reshape view whenever the user modifies the viewport window.
 width - new width in pixels.
 height - new height in pixels.
 **********************************************************************************/
void Arthropoda::reshapeView( int width, int height )
{
	if( height == 0 )									//Prevent division by zero.
		height = 1;
    
	viewportWidth = width;
	viewportHeight = height;
    
	float ratio = (float)viewportWidth/(float)viewportHeight;
    
	//Write on console.
	ATcl::outputMessage( "New aspect ratio %f for width %d and height %d", ratio, viewportWidth, viewportHeight );
    
	glViewport( 0, 0, viewportWidth, viewportHeight );	//Change viewport.
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();
    
	//This defines the field of view of the camera.
	//Making the first 4 parameters larger will give a larger field of
	//view, therefore making the objects in the scene appear smaller.
	//glOrtho( -ratio*0.03, ratio*0.03, -0.03, 0.03, -1.0, 1.0);
    
	// Use either of the following functions to set up a perspective view
	gluPerspective( 45, (float)viewportWidth/(float)viewportHeight, 0.01, 10 );
	//glFrustum(-1,1,-1,1,4,100);
    
	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();
    
	//This sets the virtual camera:
	//gluLookAt( x,y,z,   x,y,z   x,y,z );
	//           camera  look-at camera-up
	//           pos'n    point   vector
	gluLookAt( eye[0], eye[1], eye[2], reference[0], reference[1], reference[2], 0.0, 1.0, 0.0 );
    
	HMatrix arcBallRot;
	Ball_Value( arcBall, arcBallRot );
	glMultMatrixf( (float*)arcBallRot );
    
	glutPostRedisplay();
}

/***************************************************************************************************
 Callback function to handle mouse buttons being pressed.
 button - pressed mouse button.
 state - if mouse button was pressed or released.
 x - x coordinate of current mouse location.
 y - y coordinate of current mouse location.
***************************************************************************************************/
void Arthropoda::mousePressed( int button, int state, int x, int y )
{
	mouseButton = button;
    
	if( mouseButton == GLUT_LEFT_BUTTON && state == GLUT_DOWN )
	{
		HVect arcball_coords;
		arcball_coords.x = 2.0*(float)x/(float)viewportWidth - 1.0;
		arcball_coords.y = -2.0*(float)y/(float)viewportHeight + 1.0;
		Ball_Mouse( arcBall, arcball_coords ) ;
		Ball_Update( arcBall );
		Ball_BeginDrag( arcBall );
	}
    
	if( mouseButton == GLUT_LEFT_BUTTON && state == GLUT_UP )
	{
		Ball_EndDrag( arcBall );
		mouseButton = -1;
	}
    
	if( mouseButton == GLUT_RIGHT_BUTTON && state == GLUT_DOWN )
		previousY = y;
    
	if( mouseButton == GLUT_RIGHT_BUTTON && state == GLUT_UP )
		mouseButton = -1;
    
	//Tell the system to redraw the window.
	glutPostRedisplay();
}

/***************************************************************************************************
 Callback function to handle mouse being moved.
 x - x coordinate of current mouse location.
 y - y coordinate of current mouse location.
***************************************************************************************************/
void Arthropoda::mouseMoved( int x, int y )
{
	if( mouseButton == GLUT_LEFT_BUTTON )
	{
		HVect arcball_coords;
		arcball_coords.x = 2.0*(float)x/(float)viewportWidth - 1.0 ;
		arcball_coords.y = -2.0*(float)y/(float)viewportHeight + 1.0;
		Ball_Mouse( arcBall, arcball_coords );
		Ball_Update( arcBall );
        
		glutPostRedisplay();	//Redraw scene.
	}
    
	else
	{
		if( mouseButton == GLUT_RIGHT_BUTTON )
		{
			if( y - previousY > 0 )
				zoom *= 1.03;
			else
				zoom *= 0.97;
            
			previousY = y;
            
			glutPostRedisplay();
		}
	}
}

/***************************************************************************************************
 Function to handle key-pressing events.
 key - key code being pressed.
 x - x coordinate of mouse location when key was pressed.
 y - y coordinate of mouse location when key was pressed.
***************************************************************************************************/
void Arthropoda::keyPressed( unsigned char key, int x, int y )
{
	switch( key )
	{
		case 'q':
		case 27:
            ode->~AOde();   //Call ODE destructor.
			exit(0);
		case 'f':			//Take a snapshot in ppm format.
			frameSaver->DumpPPM( viewportWidth, viewportHeight );
			break;
		case 'r':			//Reset view.
			restoreView();
			glutPostRedisplay();
			break ;
		case 's':			//Togle simulation state.
			simulate = !simulate;
			if( simulate )	//Reset timer.
			{
				resetTime();
				ATcl::outputMessage( "Simulation started..." );
			}
			else
				ATcl::outputMessage( "Simulation paused..." );
			break;
		case 't':
			step = true;	//Advance one simulation step.
			break;
		case 'm':			//Toggle frame dumping.
			record = !record;
			if( record )
				ATcl::outputMessage( "Frame dumping enabled" );
			else
				ATcl::outputMessage( "Frame dumping disabled..." );
			frameSaver->Toggle();
			break;
		case 'h':
		case '?':
			plotInstructions();
			break;
	}
}

/***************************************************************************************************
 Function to process special keys.
 Inputs:
    xx - x mouse coordinate when a key was pressed.
    yy - y mouse coordinate wheb a key was pressed.
***************************************************************************************************/
void Arthropoda::specialKeyPressed( int key, int xx, int yy )
{
    double fraction = 0.002;
    
    switch( key )
    {
        case GLUT_KEY_LEFT:
        {
            eye[0] = eye[0] - fraction;
            reference[0] = reference[0] - fraction;
            break;
        }
        case GLUT_KEY_RIGHT:
        {
            eye[0] = eye[0] + fraction;
            reference[0] = reference[0] + fraction;
            break;
        }
        case GLUT_KEY_UP:
        {
            eye[1] = eye[1] + fraction;
            reference[1] = reference[1] + fraction;
            break;
        }
        case GLUT_KEY_DOWN:
        {
            eye[1] = eye[1] - fraction;
            reference[1] = reference[1] - fraction;
        }
    }

    glutPostRedisplay();
}

/***************************************************************************************************
 Auxiliary function to reset the view.
***************************************************************************************************/
void Arthropoda::resetArcBall()
{
	Ball_Init( arcBall );
	Ball_Place( arcBall, qOne, 0.75 );
}

/***************************************************************************************************
 Function to plot instructions.
***************************************************************************************************/
void Arthropoda::plotInstructions()
{
	ATcl::outputMessage("Keys:");
	ATcl::outputMessage("  r - restore the original view.") ;
	ATcl::outputMessage("  f - take snapshot of current frame.") ;
	ATcl::outputMessage("  s - toggle the simulation.") ;
	ATcl::outputMessage("  m - toggle frame dumping.") ;
	ATcl::outputMessage("  q - quit.");
	ATcl::outputMessage("  h - print these instructions.");
}

/***************************************************************************************************
 Function to handle the idle event.
***************************************************************************************************/
void Arthropoda::idle()
{
    ATcl::tclTkEvents();
    
	if( simulate || step )
	{
		simulationTime += simulationStep;
        
		//Advance physics-based simulation.
        ode->simulationLoop();
        
		glutPostRedisplay();
		
		step = false;			// Disable single simulation step.
	} 
}
