//
//  ASpiderSystem.h
//  Arthropoda
//
//  Created by Luis Angel on 9/22/14.
//  Copyright (c) 2014 Luis Angel. All rights reserved.
//

#ifndef __Arthropoda__ASpiderSystem__
#define __Arthropoda__ASpiderSystem__

/***************************************************************************************************
 Class that implements the Spider system.
***************************************************************************************************/

#include "ABaseSystem.h"
#include "ATcl.h"
#include "ADrawing.h"
#include "ACurve.h"
#include <assert.h>
#include <strings.h>
#include <iomanip>

using namespace ACurveNS;

// How many groups of leg objects do we consider?
#define LEG_COUNT			8

// Maximum force applied to a joint to reach a desired velocity.
#define MAX_JOINT_FORCE				0.1
#define JOINT_FORCE_STRONG_FACTOR	4.0

// Maximum velocity for joints.
#define MAX_JOINT_VEL				30.0

// Percentage of total distance to allow stop detection.
#define MIN_DISTANCE_STOP_PERCENT   0.02

// Minimum distance, maximum depth, and decceletarion threshold to activate a claw.
#define MIN_DISTANCE_CLAW_CONTACT   5e-6
#define MAX_DEPTH_CLAW_CONTACT      6e-4
#define DECELERATION_THRESHOLD		-0.5
#define MAX_CLAW_FORCE				10.0

// Profile height (y-coordinate in prosoma coordinates)
#define PROFILE_HEIGHT      -0.01

// Stepping parameters.
#define SEARCH_STEP_SPEED	0.3
#define IK_BASE_SPEED		0.1
#define MAX_STEP_SPEED		0.7
#define EASING_IN_PERCENT	0.1
#define EASING_OUT_PERCENT	0.1
#define MAX_TURNING_ANGLE	PI/60			// 3 degrees.

// Body parts.
#define TRUNK				0
#define L_LEG1				1
#define L_LEG2				2
#define L_LEG3				3
#define L_LEG4				4
#define R_LEG1				5
#define R_LEG2				6
#define R_LEG3				7
#define R_LEG4				8

// Trunk links.
#define OPISTHOSOMA			0
#define PROSOMA				1

// Trunk joints.
#define  PEDICEL			0

// Leg links.
#define FEMUR				0
#define TIBIA				1
#define METATARSUS			2
#define TARSUS				3

// Leg joints.
#define COXA				0
#define PATELLA				1
#define KNEE				2
#define ANKLE				3
#define CLAW				4

// Joint axes.
#define AXIS_1				0
#define AXIS_2				1

// Joint limits.
#define LO_STOP				0
#define HI_STOP				1

// Leg states.
#define S_INIT				0							// Initializing leg positioning/rotation.
#define S_READY				1							// Leg is still, awaiting for a command.
#define S_STANCE			2							// Leg is grounded, supporting motion of prosoma.
#define S_STEPPING			3							// Leg is in swing, following an IK path.
#define S_IMBALANCE			4							// Error: foot is beyond profile sphere.
#define S_RESTRICTION		5							// Error: a segment, other than the foot, hit the environment.
#define S_OVERSTRETCHING	6							// Error: leg joint angles are nearly zero.
#define S_LIMBO				7							// Error: leg terminated path (from S_STEPPING) but did not get grounded.

// Height regulation.
#define HEIGHT_REST_LENGTH	0.02						// Spring rest length.
#define HEIGHT_KP			200.0						// Spring constant.
#define HEIGHT_KD			5.0							// Damping constant.

// Follow regulation.
#define FOLLOW_KP			500.0						// Spring constant.
#define FOLLOW_KD			1.0							// Damping constant.

// Orientation regulation.
#define ORIENTATION_KP		10.0						// Spring constant.

// Class inherits from the abstract class ABaseSystem.
class ASpiderSystem: public ABaseSystem
{
private:
    struct IKStruct										// Struct to store information about groups of
    {													// parts in the spider we can move or stop in the simulation.
        ACurve hermiteCurve;							// Parametric curve.
        double ikTime;									// Time control for IK on this body part.
        double fMax[2];									// Enable/Disable joints motors.
        int state;										// Motion state of this leg.
        double oldCumulativeVelocity;                   // Accounts for the cumulative velocity of this leg.
        double newCumulativeVelocity;
        double minimumDistanceToStop;                   // After this distance, we can test when the body part comes to a stop.
		double traveledDistance;						// Distance traveled along a parametric curve.
    };
	
	enum EndPoint { EP_BASE, EP_TIP };					// Link End Point: base or tip.
    
    /////////////////////////////////////////// Biometrics /////////////////////////////////////////
    
    // Opisthosoma and prosoma mass measurements.
    const double opisthosomaMass = 0.01764;             //Total spider mass = 0.1764g
    const double prosomaMass = 0.01764;
    
    // Leg measurement constants.
    //                                    N/A     L1      L2      L3      L4      R1      R2      R3      R4
    const double femurLengths[9] =      { 0.0,   0.06,   0.06,   0.06,   0.06,   0.06,   0.06,   0.06,   0.06 };
	const double tibiaLengths[9] =      { 0.0,  0.062,  0.054,   0.03,   0.05,  0.062,  0.054,   0.03,   0.05 };
    const double metatarsusLengths[9] = { 0.0,  0.042,  0.042,  0.042,  0.042,  0.042,  0.042,  0.042,  0.042 };
    const double tarsusLengths[9] =     { 0.0,  0.015,  0.015,  0.015,  0.015,  0.015,  0.015,  0.015,  0.015 };
    //									    Femur     Tibia  Metatarsus  Tarsus
    const double legLinkRadii[4] =      {   0.0035,   0.0025,   0.0015,    0.001 };
    const double legLinkMass[4]  =      { 0.031459, 0.022471, 0.013482, 0.008988 };	//Mass per leg = 0.0764
	
	// Legs joint angular limits.
	//									       ------- LO_STOP ------	  ----- HI_STOP -----
	//									          AXIS_1	  AXIS_2        AXIS_1    AXIS_2
	const double legJointLimits[5][2][2] = { { { -1.116298, -0.610865 }, { 0.716298, 0.610865 } },		// COXA.
					   				         { {  0.0     , -0.610865 }, { 2.792527, 0.610865 } },		// PATELLA.
									         { { -1.09083 , -1.308997 }, { 1.09083 , 1.308997 } },		// KNEE.
									         { { -1.09083 , -0.567232 }, { 1.09083 , 0.567232 } },		// ANKLE.
									         { { -PI/2.0  , -PI/2.0   }, { PI/2.0  , PI/2.0   } }		// CLAW
									       };
	
	// Trunk joint angular limits.
	//									   Only one axis
	const double trunkJointLimits[2] = { -PI/6.0, PI/6.0 };		// Pedicel.
    
    // Initial desired angles.
    const vector< double >initDesiredLegAngles = vector< double >{ -5.3*PI/18.0, PI/3.0, 5.2*PI/18.0, PI/4.0 };
    
    ////////////////////////////// Private Member Functions and Properties /////////////////////////
    
    dWorldID world;                                     // Reference to ODE world.
    dSpaceID space;                                     // Reference to ODE space.
    double timeStep;                                    // Simulation time step.
	
	bool beganFalling;									// Used to prevent a velocity zero at the very beginning of the simulation.
    
    vector< vector< AObject > > objects;                // The rigid objects that make up the creature.
    vector< vector< AJoint > > joints;                  // The joints that make up the whole body.
    vector< vector< unsigned char > > collisionCodes;   // Codes assigned to each geometry in the whole body.
    AVector3D prosomaLocation;                          // Root of the whole body.
    
    // Spider profile.
    //
    double legsComfortAngles[9];                        // Legs comfort angles.
    AVector3D profile[9];                               // Legs comfort locations.
    const double profileSphereRadius = 0.03;
    bool showProfileSpheres;                            // True or false: displaying the profile spheres.
    
    // Inverse Kinematic parameters.
    //
    vector< IKStruct > ikBodyParts;                     // A vector of IK structs.
    bool showHermiteCurves;                             // True or false: display parametric curves.
	
	// Step sequence.
	vector< pair< int, int > > steppingSequence;		// A linked list of pairs (legID, delay).
	int walk;											// Walk int flag: 0 - stop, walk > 0 means speed = 0.1 * walk.
	int walkDelay;										// Number of time steps that leg at front of stepping sequence will start swing.
	int nLegsStepping;									// Number of legs stepping at all time.
	AVector3D rFollowOld;								// Distance from current prosoma center to expected prosoma center.
	AVector3D rFollow;									// Stores the most up-to-date distance for walking force.
	
	// Orientation and height regulation.
	double rHeightOld;									// Distance from current prosoma height to expected prosoma height.
	double rHeight;										// Stores the most up-to-date diatance for height regulation.
    
    void initOpisthosoma();
    void initProsoma( const double* prosomaRadii, const double* prosomaLengths );
    void initLeg( int LL, double angleL, double angleX, double prosomaLinkRadius, double prosomaLinkLength, const double* jointRadius );
    AVector3D getWEndPointOf( int leg, int link, EndPoint ep );
    AVector3D getWorldFromProsomaCoordinates( const AVector3D& P );
    double jacobianInverseKinematics( int L );
    void changeDesiredJointAngle( int L, int joint, int axis, double deltaAngle );
    void maintainDesiredAngles();
    void regulationModule();
    void steppingModule();
    bool legHasStopped( int L );
	bool legIsGrounded( int L );
	double getFootToProfileDistance( int L );
	double setEllipseOnLeg( int L, const AVector3D direction = zeroVector3D, const AVector3D normal = zeroVector3D, double height = 0, double speed = 0 );
	void getClawMatrices( const dJointID claw, AVector3D& T, AMatrix< 3, 3 >& R );
	AVector3D getWorldPointFromLocalClaw( const dJointID claw, const AVector3D pl );
	AVector3D getLocalPointFromWorldClaw( const dJointID claw, const AVector3D pw );
	AVector3D getWorldVectorFromLocalClaw( const dJointID claw, const AVector3D vl );
	AVector3D getLocalVectorFromWorldClaw( const dJointID claw, const AVector3D vw );
	void heightRegulationModule();
	void orientationModule();
	void anchoredLegsPerSide( vector< AVector3D >&l, vector< AVector3D >&r, vector< AVector3D >&f, vector< AVector3D >&b );
	void walkTowards( const AVector3D& direction );
	int contralateralLeg( int L );
    
    /////////////////////////////////////// Public interface ///////////////////////////////////////
    
public:
    ASpiderSystem( dWorldID w, dSpaceID s, double deltaT );
    int command( int objc, Tcl_Obj *CONST objv[] );
    void drawSystem();
    void setSpiderProfile( double radius );
    unsigned char encodeCollisionCode( int group, int member );
    void decodeCollisionCode( unsigned char code, int* group, int* member );
    void simulate();
    bool inCollision( int part, int link, dGeomID targetGeom, const vector< dContactGeom >& contactInfo );
    AVector3D getPosition();
	void updateVelocities();
	string dumpAngles( int leg );
};

#endif /* defined(__Arthropoda__ASpiderSystem__) */
